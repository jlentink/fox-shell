module.exports = function(grunt) {
	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		uglify: {
			options: {
				banner: '/*! <%= pkg.name %> <%= pkg.version %> <%= grunt.template.today("yyyy-mm-dd HH:MM:ss") %> */\n',
				sourceMap: 'public/js/<%= pkg.name %>.map',
				sourceMapRoot: '/js/',
				sourceMapPrefix: 3,
				sourceMappingURL: '/js/<%= pkg.name %>.map',
//				sourceMapIn: 'public/js/src/'
			},
			build: {
				expand: true,
				src: 'public/js/src/**/*.js',
				dest: 'public/js',
				ext: '.min.js',
				flatten: true
			}
	    },
		jshint: {
				files: [
				        'public_src/javascripts/src/**/*.js',
				        '!public_src/javascripts/src/vendor/**/*.js'
				       ],
				options: {
					browser: true,
					jquery: true,
					smarttabs: true
				}
		},

		compress: {
			main: {
				options: {
					mode: 'gzip'
				},
				files: [
					{
						expand: true,
						cwd: 'public/js/',
						src: ['*.min.js'],
						dest: 'public/js/',
						ext: '.min.js.gz'
					}
				]
			}
		},

		removelogging: {
			src: "public_src/javascripts/concat/**/*.js"
		},

		compass: {
			dev: {
				options: {
					sassDir: 'public_src/sass',
					cssDir: 'public/css',
					httpPath: '/airmiles/',
					force: false,
					imageDir: 'img',
					imagesPath: 'public/img/',
//					relative_assets: true,
					generatedImagesPath: 'public/img/',
					javascriptsPath: 'public/js/',
					outputStyle: 'nested' // nested, expanded, compact, compressed
				}
			},
			prod: {
				options: {
					sassDir: 'public_src/sass',
					cssDir: 'public/css',
					httpPath: '/airmiles/',
					force: true,
					imageDir: 'img',
					imagesPath: 'public/img/',
					generatedImagesPath: 'public/img/',
//					relative_assets: true,
					javascriptsPath: 'public/js/',
					outputStyle: 'compressed' // nested, expanded, compact, compressed
				}
			}

		},

		copy: {
			default: {
				expand: true,
				cwd: 'public_src/javascripts/concat/',
				src: '*.js',
				dest: 'public/js/src/',
				flatten: true,
				filter: 'isFile'
			}
		},

		watch: {
			js:  {
				files: 'public_src/javascripts/src/**/*.js',
				tasks: [ 'genjs' ]
			},
			css:  {
				files: 'public_src/sass/**/*.scss',
				tasks: [ 'gencss' ]
			},
		}
	});


	// Concat Task
	grunt.registerTask('preparemodulejs', 'iterates over all module directories and compiles modules js files', function() {
		grunt.file.expand("public_src/javascripts/src/*").forEach(function (dir) {
		    // get the current concat config
		    var concat = grunt.config.get('concat') || {};
//		    var copy = grunt.config.get('copy') || {};
			var dirsplit = dir.split('/');
		    concat[dir] = {
		    	src: dir + '/**/*.js',
		    	dest: 'public_src/javascripts/concat/' + dirsplit[dirsplit.length-1] + '.js'
		    }
		    //'public_src/javascripts/concat/' + dirsplit[dirsplit.length-1] + '
//		    copy.default.files[copy.default.files.length] = {cwd: 'public_src/javascripts/concat/', flatten: true, src: ['*.js'], dest: 'public/js/', expand: false, filter: 'isFile'};
//			grunt.log.writeln(copy.default.files.length);
//			grunt.log.writeflags(copy, '');
//			grunt.config.set('copy', copy)
			grunt.config.set('concat', concat);
		});
	});

	// Load the plugin that provides the "uglify" task.
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-compass');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-remove-logging');
	grunt.loadNpmTasks('grunt-contrib-compress');

	// Default task(s).
	grunt.registerTask('genjs', ['jshint', 'preparemodulejs:dev','concat', 'copy', 'uglify', 'compress']);
	grunt.registerTask('gencss', ['compass:dev']);
	grunt.registerTask('prod', ['compass:prod', 'jshint', 'preparemodulejs:prod','concat', 'copy', 'removelogging', 'uglify', 'compress']);
	grunt.registerTask('default', ['genjs', 'gencss']);

};
