<?php
    // config/autoload/doctrine.global.php
    return array(
		'doctrine' => array(
    		    'dbal' => array(
    		    		'connections' => array(
    		    				'default' => array(
    		    						'mapping_types' => array(
    		    								'enum'  => 'string',
    		    						),
    		    				),
    		    		),
    		    ),		    
				'connection' => array(
						'orm_default' => array(
								'driverClass' => 'Doctrine\DBAL\Driver\PDOMySql\Driver',
								'params' => array(
										'host' => '127.0.0.1',
										'port' => '3306',
										'dbname' => 'fox',
								        'user' => 'root',
								        'password' => '123456',								    
								),
						),
				)
		));