<?php
return array(
    'airmiles' => array(
    		'sequencenumber' => array(
    				'protocol' => 'tcp://',
    				'host' => '127.0.0.1',
    				'port' => 5050,
    		),
    		'saldochecker' => array(
    				'environment' => 'test',
    				'production' => 'http://212.136.0.230/saldochecker/saldochecker.asmx',
    				'staging' => 'http://212.136.0.231/saldochecker/saldochecker.asmx',
    				'test' => 'http://static.xeed.nl/airmiles-proxy/',
    				'wsdl' => 'Saldochecker_20120717.wsdl'
    		),
    		'partnerdetails' => array(
    				'suppliercode' => 'FOBEAT',
    				'branchnumber' => '1',
    				'posnumber' => '1',
    				'productcode' => 'FOX2M2SHIRTS',
    				'airmilesprice' => '1'
    		),
    ),
    'service_manager' => array(
        'invokables' => array(
            'Voucher\Model\Purchase' => 'Voucher\Model\Purchase',
            'Voucher\Model\Saldochecker' => 'Voucher\Model\Saldochecker',
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Voucher\Controller\Voucher' => 'Voucher\Controller\VoucherController',
        ),
    ),
    'doctrine' => array(
    		'driver' => array(
    				'voucher_entities' => array(
    						'class' =>'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
    						'cache' => 'array',
    						'paths' => array(__DIR__ . '/../src/Voucher/Entity')
    				),

    				'orm_default' => array(
    						'drivers' => array(
    								'Voucher\Entity' => 'voucher_entities'
    						)
    				)
    		    )
    ),
    'router' => array(
        'routes' => array(
            'voucher' => array(
                'type'    => 'Literal',
                'options' => array(
                    // Change this to something specific to your module
                    'route'    => '/',
                    'defaults' => array(
                        // Change this value to reflect the namespace in which
                        // the controllers for your module are found
                        '__NAMESPACE__' => 'Voucher\Controller',
                        'controller'    => 'Voucher',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                    		'type'    => 'Literal',
                    		'options' => array(
                    				// Change this to something specific to your module
                    				'route'    => '',
                    				'defaults' => array(
                    						// Change this value to reflect the namespace in which
                    						// the controllers for your module are found
                    						'__NAMESPACE__' => 'Voucher\Controller',
                    						'controller'    => 'Voucher',
                    						'action'        => 'index',
                    				),
                    		),
                    ),
                    'aankopen' => array(
                    		'type'    => 'Literal',
                    		'options' => array(
                    				// Change this to something specific to your module
                    				'route'    => 'aankoop',
                    				'defaults' => array(
                    						// Change this value to reflect the namespace in which
                    						// the controllers for your module are found
                    						'__NAMESPACE__' => 'Voucher\Controller',
                    						'controller'    => 'Voucher',
                    						'action'        => 'uwbestelling',
                    				),
                    		),
                    ),
                    'loginairmiles' => array(
                    		'type'    => 'Literal',
                    		'options' => array(
                    				// Change this to something specific to your module
                    				'route'    => 'login-am',
                    				'defaults' => array(
                    						// Change this value to reflect the namespace in which
                    						// the controllers for your module are found
                    						'__NAMESPACE__' => 'Voucher\Controller',
                    						'controller'    => 'Voucher',
                    						'action'        => 'loginaimriles',
                    				),
                    		),
                    ),
                    'voorwaarden' => array(
                    		'type'    => 'Literal',
                    		'options' => array(
                    				// Change this to something specific to your module
                    				'route'    => 'voorwaarden',
                    				'defaults' => array(
                    						// Change this value to reflect the namespace in which
                    						// the controllers for your module are found
                    						'__NAMESPACE__' => 'Voucher\Controller',
                    						'controller'    => 'Voucher',
                    						'action'        => 'voorwaarden',
                    				),
                    		),
                    ),
                    'privacy' => array(
                    		'type'    => 'Literal',
                    		'options' => array(
                    				// Change this to something specific to your module
                    				'route'    => 'privacy',
                    				'defaults' => array(
                    						// Change this value to reflect the namespace in which
                    						// the controllers for your module are found
                    						'__NAMESPACE__' => 'Voucher\Controller',
                    						'controller'    => 'Voucher',
                    						'action'        => 'privacy',
                    				),
                    		),
                    ),
                    'login' => array(
                    		'type'    => 'Literal',
                    		'options' => array(
                    				// Change this to something specific to your module
                    				'route'    => 'logintest',
                    				'defaults' => array(
                    						// Change this value to reflect the namespace in which
                    						// the controllers for your module are found
                    						'__NAMESPACE__' => 'Voucher\Controller',
                    						'controller'    => 'Voucher',
                    						'action'        => 'login',
                    				),
                    		),
                    ),
                    'loginclose' => array(
                    		'type'    => 'Literal',
                    		'options' => array(
                    				// Change this to something specific to your module
                    				'route'    => 'loginclose',
                    				'defaults' => array(
                    						// Change this value to reflect the namespace in which
                    						// the controllers for your module are found
                    						'__NAMESPACE__' => 'Voucher\Controller',
                    						'controller'    => 'Voucher',
                    						'action'        => 'loginclose',
                    				),
                    		),
                    ),
                    'email' => array(
                    		'type'    => 'Segment',
                    		'options' => array(
                    				// Change this to something specific to your module
                    				'route'    => 'email/:voucher/:password',
                        		    'constraints' => array(
                        		    		'password'        => '([a-zA-Z0-9]+)',
                        		    ),
                    				'defaults' => array(
                    						// Change this value to reflect the namespace in which
                    						// the controllers for your module are found
                    						'__NAMESPACE__' => 'Voucher\Controller',
                    						'controller'    => 'Voucher',
                    						'action'        => 'email',
                    				),
                    		),
                    ),
               		'status' => array(
						'type'    => 'Literal',
						'options' => array(
							// Change this to something specific to your module
							'route'    => 'status',
							'defaults' => array(
								// Change this value to reflect the namespace in which
								// the controllers for your module are found
								'__NAMESPACE__' => 'Voucher\Controller',
								'controller'    => 'Voucher',
								'action'        => 'status',
							),
						),
               		),
               		'sendreminder' => array(
						'type'    => 'Literal',
						'options' => array(
							// Change this to something specific to your module
							'route'    => 'sendreminder',
							'defaults' => array(
								// Change this value to reflect the namespace in which
								// the controllers for your module are found
								'__NAMESPACE__' => 'Voucher\Controller',
								'controller'    => 'Voucher',
								'action'        => 'sendreminder',
							),
						),
               		),
                    'email-test' => array(
                    		'type'    => 'Segment',
                    		'options' => array(
                    				// Change this to something specific to your module
                    				'route'    => 'emailtest/:password',
                        		    'constraints' => array(
                        		    		'password'        => '([a-zA-Z0-9]+)',
                        		    ),
                    				'defaults' => array(
                    						// Change this value to reflect the namespace in which
                    						// the controllers for your module are found
                    						'__NAMESPACE__' => 'Voucher\Controller',
                    						'controller'    => 'Voucher',
                    						'action'        => 'emailtest',
                    				),
                    		),
                    ),
                    'uwvouchercode' => array(
                    		'type'    => 'Segment',
                    		'options' => array(
                    				// Change this to something specific to your module
                    				'route'    => 'uwvouchercode[/:force/:time]',
                    				'defaults' => array(
                    						// Change this value to reflect the namespace in which
                    						// the controllers for your module are found
                    						'__NAMESPACE__' => 'Voucher\Controller',
                    						'controller'    => 'Voucher',
                    						'action'        => 'uwvouchercode',
                    				),
                    		),
                    )

/*
                    'voucher' => array(
                    		'type'    => 'Literal',
                    		'options' => array(
                    				// Change this to something specific to your module
                    				'route'    => '/voucher/uwvouchercode/bevestigd/[]',
                    				'defaults' => array(
                    						// Change this value to reflect the namespace in which
                    						// the controllers for your module are found
                    						'__NAMESPACE__' => 'Voucher\Controller',
                    						'controller'    => 'Voucher',
                    						'action'        => 'index',
                    				),
                    		),
                        ),

    					'default' => array(
    							'type'    => 'Segment',
    							'options' => array(
    									'route'    => '[/][:action[/:code]]',
    									'constraints' => array(
    											'action'     => '[a-zA-Z0-9][a-zA-Z0-9_-]*',
    											'code'        => '[a-zA-Z0-9][a-zA-Z0-9_-]*',
    									),
    									'defaults' => array(
    											'controller' => 'Voucher',
    											'action'        => 'index',
    									),
    							),
    					),
*/
                    ),

            ),
        ),
    ),
    'view_manager' => array(
        'strategies' => array(
        		'ViewJsonStrategy',
        ),
        'template_path_stack' => array(
            'Voucher' => __DIR__ . '/../view',
        ),
    ),
);
