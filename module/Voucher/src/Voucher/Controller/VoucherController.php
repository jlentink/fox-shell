<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Voucher for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Voucher\Controller;

use Zend\Mvc\Controller\AbstractActionController,
	Zend\View\Model\ViewModel;
use Zend\InputFilter\Input;
use Zend\InputFilter\InputFilter;
use Zend\Session\SessionManager;
use Zend\Session\Storage\SessionStorage;
use Zend\Session\Container;
use Voucher\Model\Purchase;
use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp;
use Zend\Mail\Transport\SmtpOptions;
use Zend\View\Model\JsonModel;
use Zend\Http\Response;
use Zend\Json\Json;
use Voucher\Model\Login\webservice;
use Voucher\Model\Soap\Elements\GetSaldoWithSecurityPostalCode;
use Voucher\Model\Soap\Complextype\SimpleDate;
use Voucher\Model\Saldochecker;
use Zend\Validator\EmailAddress;
use Zend\Form\Element\Email;

class VoucherController extends AbstractActionController
{
    // Return errors to Form
    //   1 = unsufficient funds
    //   2 = card blocked
    //   6 = unvalid combination
    //   9 = unkown card
    //  99 = invalid form
    // 102 = out of action codes
    // 103 = once is used
    // 104 = Cookies error
    // 105 = Airmiles is already used without force

    const ERROR_NONE = 0;
    const ERROR_AIRMILES_UNSUFFICIENT_FUNDS = 1;
    const ERROR_AIRMILES_CARD_BLOCKED = 2;
    const ERROR_AIRMILES_UNKOWN_COMBINATION = 6;
    const ERROR_AIRMILES_UNKOWN_CARD = 9;
    const ERROR_INVALID_FORM = 99;
    const ERROR_CANNOT_CREATE_CAR = 101;
    const ERROR_OUT_OF_VOUCHERS = 102;
    const ERROR_ONCE_USED = 103;
    const ERROR_COOKIE_ERROR = 104;
    const ERROR_CARD_USERD_WITHOUT_FORCE = 105;
    const ERROR_INVALID_EMAIL = 9000;
    const ERROR_ONCE_INCORRECT = 9001;

    protected $em;

    public function indexAction()
    {
        $config = $this->getServiceLocator()->get('Config');

        $startDate = new \DateTime('now');
        $endDate   = new \DateTime($config['campaign']['enddate']);
        $diff = $startDate->diff($endDate);
    
        if($config['campaign']['enabled'] == false || $endDate->getTimestamp() < time()){
            $view = new ViewModel(array());
            $view->setTemplate('voucher/voucher/index-disabled.phtml'); // path to phtml file under view folder
            return $view;
        }
        return array('daysleft' => $diff->format('%a'));
    }

    public function loginaimrilesAction(){
        $usernameInput				= $this->params()->fromPost('username', null);
        $passwordInput 				= $this->params()->fromPost('password', null);
        $config                     = $this->getServiceLocator()->get('Config');
        $airmilesuser               = new Container('AirMilesData');
        $data = array(
            'result' => true,
            'data' => array()
        );


        $webservice = new webservice();
        $webservice->setBinding($config['airmiles']['login'][$config['airmiles']['login']['environment']]);
        $webservice->setUsername($config['airmiles']['login']['username']);
        $webservice->setPassword($config['airmiles']['login']['password']);


        $MT_Memberinformation_NewPartnerLogon_Request = $webservice->getLoginObject();
        $MT_Memberinformation_NewPartnerLogon_Request->Member->Partner = 'FOBEAT'; //Set partner Id
        $MT_Memberinformation_NewPartnerLogon_Request->Member->UserID = $usernameInput; // Set Airmiles Username
        $MT_Memberinformation_NewPartnerLogon_Request->Member->Password = $passwordInput; //Set Airmiles Password

        $result = $webservice->SIOS_Memberinformation_NewPartnerLogon($MT_Memberinformation_NewPartnerLogon_Request);

        if ($result->Member_Header->Member->UserStatus !== 'P00'){
            $data['result'] = false;
            $data['data']['error'] = $result->Member_Header->Member->UserStatus;

        }else {
            //$once = microtime(true);
            $data['result'] = true;
            $postcode = str_replace(' ', '', $result->Member_Header->Member->Address->PostalCode);
            $housenumber = $result->Member_Header->Member->Address->HouseNumber;
            $cardnumber = $result->Member_Header->Card->CardNumber;



            $saldoChecker = $this->getServiceLocator()->get('Voucher\Model\Saldochecker');
            $saldoChecker->setAirmiles($cardnumber);
            $saldoChecker->setPostcode($postcode);
            $saldoChecker->setHouseNr($housenumber);

            $saldo =$saldoChecker->getSaldo()->getData();
            
            
            $airmilesuser->result = $result;
            $airmilesuser->postcode = $postcode;
            $airmilesuser->housenr  = $housenumber;
            //$airmilesuser->once  = $once;
            $airmilesuser->card  = $cardnumber;
            $airmilesuser->saldo  = $saldo;
            $airmilesuser->email  = '';
            
            $data['data']['saldo'] = $saldo;
            $data['data']['amnr'] = $cardnumber;
            //$data['data']['once'] = $once;
        }

        $repsonse = $this->getResponse()->setContent(Json::encode($data));
        $repsonse->getHeaders()->addHeaderLine('Content-Type', 'text/json');
        return $repsonse;
    }

    public function uwbestellingAction()
    {
        $config = $this->getServiceLocator()->get('Config');
        $airmilesuser = new Container('AirMilesData');
        if($config['campaign']['enabled'] == false){
             $this->redirect()->toUrl('/');
        }


        $data = array(
        		'airmilesnumber' => '',
        		'zipcode' => '',
        		'housenumber' => '',
        		'housenumberpostfix' => '',
                'email' => '',
                'email_check' => '',
                'error' => 0,
                'once' => ''
        );
        $saldo = '';
        $card = '';
        $email = '';
        
        if($airmilesuser->card)
            $card = $airmilesuser->card;
        
        if($airmilesuser->saldo)
            $saldo = $airmilesuser->saldo;
        
        if($airmilesuser->email)
            $email = $airmilesuser->email;
            
        
        
        if($airmilesuser->once) {
            $once = $airmilesuser->once;
        } else {
            $once = $airmilesuser->once = time();
        }
        $data['once'] =  $once;

        if(!$data['error'] && $this->params()->fromQuery('e', null)) {
            if($this->params()->fromQuery('t', null) && (time() - $this->params()->fromQuery('t', null)) <= 10){
                $data['error'] = $this->params()->fromQuery('e', null);
            } else if($this->params()->fromQuery('t', null) == null){
                $data['error'] = $this->params()->fromQuery('e', null);
            }

        }
        // Tmp
        $data['error'] = $this->params()->fromQuery('e', null);
        
    	return array('formdata' => $data, 'card' => $card, 'saldo' => $saldo, 'email' => $email);

    }


    public function voorwaardenAction() {

    }

    public function privacyAction() {

    }
    public function uwvouchercodeAction()
    {

        //		$airmilesInput				= $this->params()->fromPost('airmilesnumber', null);
        //		$postcodeInput 				= $this->params()->fromPost('zipcode', null);
        //		$housenumberInput			= $this->params()->fromPost('housenumber', null);
        //		$housenumberSuffixInput 	= $this->params()->fromPost('housenumberpostfix', null);
        //		$emailVerificationInput		= $this->params()->fromPost('email_check', null);
        //		
        //		$forcetime            		= $this->params()->fromRoute('time', null);
        //		$formSession                = new Container('requestForm');

        //		$airmilesuser->result = new \stdClass();
        //		$airmilesuser->postcode = '1234AA';
        //		$airmilesuser->housenr  = '12';
        //		$airmilesuser->once  = microtime(true);

        
        $config = $this->getServiceLocator()->get('Config');
        $emailInput		 			= $this->params()->fromPost('email', null);
		$onceInput            		= $this->params()->fromPost('once', null);
        $airmilesuser               = new Container('AirMilesData');
		$purchase                   = $this->getServiceLocator()->get('Voucher\Model\Purchase');
		$force                		= $this->params()->fromRoute('force', null);
		$cookiesAllowed             = false;
		
		

		$endDate   = new \DateTime($config['campaign']['enddate']);
		if($config['campaign']['enabled'] == false || (time() - $endDate->getTimestamp()) > 0){
		   return $this->redirect()->toRoute('voucher/default', array(), array('query' => array('t' => time())));
		}
					
		if($airmilesuser->sessionStarted == 1){
		    $this->appendToDataInSession(array('error' => self::ERROR_COOKIE_ERROR));
		    return $this->redirect()->toRoute('voucher/aankopen', array(), array('query' => array('e' => self::ERROR_COOKIE_ERROR, 't' => time())));
		}
		
		if($force){
		    $emailInput = $airmilesuser->email;
		    $onceInput = $force;
		}

		if(!$airmilesuser->card){
		    return $this->redirect()->toRoute('voucher/default', array(), array('query' => array('t' => time())));
		}
		
		
		$emailValidator = new EmailAddress();
		if(null === $emailInput || !$emailValidator->isValid($emailInput)){
            return $this->redirect()->toRoute('voucher/aankopen', array(), array('query' => array('e' => self::ERROR_INVALID_EMAIL, 't' => time())));
		}
		$airmilesuser->email = $emailInput;

		
		if((string) $onceInput != (string) $airmilesuser->once){
		    return $this->redirect()->toRoute('voucher/aankopen', array(), array('query' => array('e' => self::ERROR_ONCE_INCORRECT, 't' => time())));
		}
		
		$purchase->setAirmiles($airmilesuser->card);
		$purchase->setEmail($emailInput);
		$purchase->setHousenumber($airmilesuser->housenr);
		$purchase->setPostcode($airmilesuser->postcode);
		$purchase->setOnce($onceInput);

		if($purchase->isOnceUsed()) {
		    return $this->redirect()->toRoute('voucher/aankopen', array(), array('query' => array('e' => self::ERROR_ONCE_USED, 't' => time())));		    
		}
		
		if($purchase->isAirmilesUsed() && !$force) {
		    $this->appendToDataInSession(array('error' => self::ERROR_CARD_USERD_WITHOUT_FORCE));
		
		    return $this->redirect()->toRoute('voucher/aankopen', array(), array('query' => array('e' => self::ERROR_CARD_USERD_WITHOUT_FORCE, 't' => time())));
		}
		
		$result = $purchase->buy();
		if(ctype_digit($result . '')){
		    $this->appendToDataInSession(array('error' => $result));
		    return $this->redirect()->toRoute('voucher/aankopen', array(), array('query' => array('e' => $result, 't' => time())));
		}
		
		$this->clearDataInSession();
		return array('code' => $result);		
		
		
//		if(!isset($airmilesuser->once) || $onceInput === $airmilesuser->once){

//		}
		//print '123q';
//		print '<pre>';
//		print_r($airmilesuser);
		//die();
		/*
		$MailSrv = $this->getServiceLocator()->get('mail-service');
		$MailSrv->setToEmail($emailInput);
		$MailSrv->setVoucherCode('SASDASSDADSADSA');
		$MailSrv->send();
		*/
		// Do Purchase
		/*
		die();


		compare $onceInput && $airmilesuser->once !!!!!


		if(is_array($_POST) && count($_POST) && isset($_POST['airmilesnumber'])){
		    $data = $_POST;
		    $this->storeDataInSession($data);
		}else if(isset($formSession->data['airmilesnumber']) && $formSession->data['airmilesnumber']){
		    $data = $formSession->data;
		} else {
		    $data = array();
		    $this->storeDataInSession($data);
		}
		*/

		//@todo: remove
 		$result = "ASDDAADSADSDASCAS";
 		return array('code' => $result);

/*
		if($formSession->sessionStarted == 1){
		    $cookiesAllowed = true;
		}

		$this->appendToDataInSession(array('error' => self::ERROR_NONE));

		$purchase->setAirmiles($data['airmilesnumber']);
		$purchase->setEmail($data['email']);
		$purchase->setHousenumber($data['housenumber']);
		$purchase->setPostcode($data['zipcode']);
		$purchase->setOnce($data['once']);

		if(!$cookiesAllowed){
			$this->appendToDataInSession(array('error' => self::ERROR_COOKIE_ERROR));
			return $this->redirect()->toRoute('voucher/aankopen', array(), array('query' => array('e' => self::ERROR_COOKIE_ERROR, 't' => time())));
		}

		if($purchase->isOnceUsed()) {
			$this->appendToDataInSession(array('error' => self::ERROR_ONCE_USED));
			return $this->redirect()->toRoute('voucher/aankopen', array(), array('query' => array('e' => self::ERROR_ONCE_USED, 't' => time())));
		}

		if($purchase->isAirmilesUsed() && null == $force) {
			$this->appendToDataInSession(array('error' => self::ERROR_CARD_USERD_WITHOUT_FORCE));

			return $this->redirect()->toRoute('voucher/aankopen', array(), array('query' => array('e' => self::ERROR_CARD_USERD_WITHOUT_FORCE, 't' => time())));
		}

		$result = $purchase->buy();
		if(ctype_digit($result . '')){
		    $this->appendToDataInSession(array('error' => $result));
		    return $this->redirect()->toRoute('voucher/aankopen', array(), array('query' => array('e' => $result, 't' => time())));
		}
		*/
		//$this->clearDataInSession();
        return array('code' => $result);
    }

    public function storeDataInSession($data){
        $formSession = new Container('requestForm');
        $formSession->data = $data;
    }

    public function clearDataInSession(){

    	$formSession = new Container('requestForm');
    	$formSession->getManager()->getStorage()->clear('requestForm');

    	$AirmilesSession = new Container('AirMilesData');
    	$formSession->getManager()->getStorage()->clear('AirMilesData');
    }

    public function appendToDataInSession(array $data){
        $formSession = new Container('requestForm');
        $formSession->data = array_merge($formSession->data, $data);
    }

    /**
     *
     * @param array $data
     * @return \Zend\InputFilter\InputFilter
     */
    protected function validateData($data){
    	$airmiles = new Input('airmilesnumber');
    	$airmiles->getValidatorChain()
        	->addValidator(new \Xeed\Validator\Airmiles());

    	$postcode = new Input('zipcode');
    	$postcode->getValidatorChain()
        	->addValidator(new \Xeed\Validator\Postcode());

    	$housenumber = new Input('housenumber');
    	$housenumber->getValidatorChain()
        	->addValidator(new \Xeed\Validator\Housenumber());


    	$housenumberSuffix = new Input('housenumberpostfix');
    	$housenumberSuffix->setAllowEmpty(true);
    	$housenumberSuffix->getValidatorChain()
        	->addValidator(new \Xeed\Validator\HousenumberPostfix());


    	$email = new Input('email');
    	$email->getValidatorChain()
        	->addValidator(new \Zend\Validator\EmailAddress())
        	->addValidator(new \Zend\Validator\Identical($data['email_check']));


    	$once = new Input('once');
    	$once->setAllowEmpty(false);
    	$once->getValidatorChain()
    	   ->addValidator(new \Zend\Validator\Regex(array('pattern' => '/^([0-9\.]+)$/')));


    	$inputFilter = new InputFilter();
    	$inputFilter
        	->add($airmiles)
        	->add($postcode)
        	->add($housenumber)
        	->add($housenumberSuffix)
        	->add($email)
        	->add($once)
        	->setData($data);

    	return $inputFilter;
    }


    public function emailAction(){
        $code = $this->params()->fromRoute('voucher');
        $password = $this->params()->fromRoute('password');

        if ('' == $code){
            return $this->notFoundAction();
        }

        $voucherObj = $this->getEntityManager()->getRepository('Voucher\Entity\Voucher');
        $voucher = $voucherObj->findOneBy(array('voucher' => $code, 'password' => $password));

        if(null == $voucher){
            return $this->notFoundAction();
        }

        $MailSrv = $this->getServiceLocator()->get('mail-service');
        $MailSrv->setVoucherCode($code);

        $response = new Response();
        $response->getHeaders()->addHeaderLine('Content-Type', 'text/html; charset=utf-8');
        $response->setContent($MailSrv->generateHTML(true));
        return $response;

    }

    /**
     *
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
    	if (null === $this->em) {
    		$this->em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
    	}
    	return $this->em;
    }


    public function emailtestAction(){
        $MailSrv = $this->getServiceLocator()->get('mail-service');
        $MailSrv->setVoucherCode('SASDASSDADSADSA');

        $response = new Response();
        $response->getHeaders()->addHeaderLine('Content-Type', 'text/html; charset=utf-8');
        $response->setContent($MailSrv->generateHTML(true));
        return $response;

    }


    public function loginAction(){

    }

    public function logincloseAction(){
    	$_SESSION['data'] = $_POST;
    	print '<html><head>';
    	print '<script type="text/javascript">window.parent.closeLogin();</script>';
    	print '</head><body></body></html>';
    	die();
    }














    /*
    public function testemailAction(){
//        $MailSrv = $this->getServiceLocator()->get('mail-service');
//        $MailSrv->sendMail("jan@januz.nl", "jan@januz.nl", "Uw Fox Sports actie code", "HYWBIQBIQ124");
//        die("verzonden");

    }



    public function goalcheckAction(){
        $lockfile = '/tmp/10000.lock';
        $count = 0;
        if(!is_file($lockfile)){

            $con = $this->getEntityManager()->getConnection();
            $stmt = $con->query('          select
                                            	COUNT(*) as `total`
                                            from
                                            	`voucher`
                                            where
                                            	`voucher`.`cartId` IS NOT NULL');

            $countRow = $stmt->fetch();
            $count = $countRow['total'];
            if($countRow['total'] >= 10000) {
                $email = new Message();
                $email->setFrom('support@xeed.nl');
                $email->addTo('paul@xeed.nl', 'Paul Zevenboom');
                $email->addTo('jason@xeed.nl', 'Jason Lentink');
                $email->addTo('log@xeed.nl', 'Log Xeed');
                $email->setBody('Fuck man een ton om een taart te kopen!');
                $email->setSubject('We hebben er 10.000!');

                $transport = new Smtp();
                $options   = new SmtpOptions(array(
                		'name'              => 'xeed',
                		'host'              => '192.168.71.200',
                		'port'              => 25
                ));
                $transport->setOptions($options);
                $transport->send($email);
                touch($lockfile);
            }
        }

        $response = $this->getResponse();
        $response->setStatusCode(200);
        $response->setContent($count);
        return $response;
    }
*/
	public function statusAction(){
	    $config = array(
	    		'accept_schemes' => 'basic',
	    		'realm'          => 'status',
	    		'digest_domains' => '/',
	    		'nonce_timeout'  => 3600,
	    );

	    $adapter = new \Zend\Authentication\Adapter\Http($config);

	    $basicResolver = new \Zend\Authentication\Adapter\Http\FileResolver();
	    $basicResolver->setFile('data/files/basicPasswd.txt');
    	$adapter->setBasicResolver($basicResolver);

    	$digestResolver = new \Zend\Authentication\Adapter\Http\FileResolver();
    	$digestResolver->setFile('data/files/basicPasswd.txt');
    	$adapter->setDigestResolver($digestResolver);

	    $adapter->setRequest($this->getRequest());
	    $adapter->setResponse($this->getResponse());

	    $result = $adapter->authenticate();
// 	    var_export($result);
// 	    var_export($this->getRequest());
// 	    die();

	    if (!$result->isValid()){
		    $viewModel = new ViewModel();
    		return $viewModel->setTemplate('voucher/voucher/status-login.phtml');
    	}

	    exec(sprintf('ping -c 1 -W 5 %s', '212.136.0.230'), $res, $rval);

	    $con = $this->getEntityManager()->getConnection();
	    $stmt = $con->query('          select
	    		COUNT(*) as `total`
	    		from
	    		`voucher`
	    		where
	    		`voucher`.`cartId` IS NOT NULL');

	    $countRow = $stmt->fetch();
	    $count = $countRow['total'];
	    return array('count' => $count, 'amstatus' => ($rval === 0));

    }

    public function statusjsonAction(){
        $config = array(
        		'accept_schemes' => 'basic',
        		'realm'          => 'status',
        		'digest_domains' => '/voucher/status',
        		'nonce_timeout'  => 3600,
        );

        $adapter = new \Zend\Authentication\Adapter\Http($config);

        $basicResolver = new \Zend\Authentication\Adapter\Http\FileResolver();
        $basicResolver->setFile('data/files/basicPasswd.txt');
        $adapter->setBasicResolver($basicResolver);

        $digestResolver = new \Zend\Authentication\Adapter\Http\FileResolver();
        $digestResolver->setFile('data/files/basicPasswd.txt');
        $adapter->setDigestResolver($digestResolver);

        $adapter->setRequest($this->getRequest());
        $adapter->setResponse($this->getResponse());
        $result = $adapter->authenticate();

        if (!$result->isValid()){
        $viewModel = new ViewModel();
        return $viewModel->setTemplate('voucher/voucher/status-login.phtml');
        }

        $con = $this->getEntityManager()->getConnection();
        $stmt = $con->query('          select
        		COUNT(*) as `total`
        		from
        		`voucher`
        		where
        		`voucher`.`cartId` IS NOT NULL');

        $countRow = $stmt->fetch();
        $count = $countRow['total'];
        $result = new JsonModel(array(
        		'count' => $count,
        		'success'=>true,
        ));



        return $result;
    }

    public function sendreminderAction(){
        set_time_limit (1000);
        $sql = '
                    SELECT
                    	airmiles, email created, voucher, voucherpassword
                    FROM
                    	`cart`
                    INNER JOIN
                    	`voucher`
                    	ON `cart`.`cartId` = `voucher`.`cartId`
                    WHERE `voucher` NOT IN (SELECT `voucher` FROM `redeemed`)
            ';

        $con = $this->getEntityManager()->getConnection();
        $membersSQL = $con->query($sql);
        $members = $membersSQL->fetchAll();
        foreach($members as $member) {

            $MailSrv = $this->getServiceLocator()->get('mail-reminder-service');
            $MailSrv->setVoucherCode($member['voucher']);
            $MailSrv->setPassword($member['voucherpassword']);
//             $MailSrv->setToEmail('jason@xeed.nl');
//             print $member['created'] . "<br />";
            $MailSrv->setToEmail($member['created']);
            $MailSrv->send();
//             print_r($member);

//             die();
        }
        die();
    }
    /*
    public function multiplebuysAction(){
    $con = $this->getEntityManager()->getConnection();
    $sql = '	SELECT airmiles
    FROM `cart`
    WHERE `cartStatus` = 2
    GROUP BY `airmiles`
    HAVING count(*) > 1';

    $membersSQL = $con->query($sql);
    $members = $membersSQL->fetchAll();

    foreach($members as $member) {
    $sql = 'SELECT *
    FROM `cart`
    WHERE `airmiles` = ' . $member['airmiles'] . '
    ORDER BY `created` ASC';
    $ordersSQL = $con->query($sql);
    $orders = $ordersSQL->fetchAll();
    print $orders[0]['airmiles'] . ';' . $orders[0]['email'];
    foreach($orders as $index => $order){
    print ';'. $order['created'];
    if($index) {
    //  print ';'. (strtotime($order['created']) - strtotime($orders[$index -1]['created']));
    print ';';
    print round((strtotime($order['created']) - strtotime($orders[$index - 1]['created'])) / 60);
    }
    //        print_r($order);
    }

    print '<br />';
    }

    die();

    }

    public function checkactivationAction(){
    $config = array(
    		'accept_schemes' => 'basic',
    		'realm'          => 'status',
    		'digest_domains' => '/voucher/status',
    		'nonce_timeout'  => 3600,
    );

    $adapter = new \Zend\Authentication\Adapter\Http($config);

    $basicResolver = new \Zend\Authentication\Adapter\Http\FileResolver();
    $basicResolver->setFile('data/files/basicPasswd.txt');
    $adapter->setBasicResolver($basicResolver);

    $digestResolver = new \Zend\Authentication\Adapter\Http\FileResolver();
    $digestResolver->setFile('data/files/basicPasswd.txt');
    $adapter->setDigestResolver($digestResolver);

    $adapter->setRequest($this->getRequest());
    $adapter->setResponse($this->getResponse());
    $result = $adapter->authenticate();

    if (!$result->isValid()){
    $viewModel = new ViewModel();
    return $viewModel->setTemplate('voucher/voucher/status-login.phtml');
    }

    if(isset($_FILES['addresses'])){
    $con = $this->getEntityManager()->getConnection();
    $membersCSV = file($_FILES['addresses']['tmp_name']);
    $membersSQL = $con->query('
    		SELECT `cart`.*, `voucher`.`voucher`
    		FROM `cart`
    		INNER JOIN  `voucher`
    		ON `cart`.`cartId` = `voucher`.`cartId`
    		WHERE
    		`cartStatus` = 2
    		AND
    		`created` <= DATE_ADD(NOW(), INTERVAL -2 DAY)
    		');



    print '<pre>';
    foreach($membersCSV as $index => $member){
    $membersCSV[$index] = trim($member);
    $membersCSV[$index] = str_replace("\n", '', $member);
    }
    $members = $membersSQL->fetchAll();
    $index = 0;
    foreach($members as $member){
    $email = strtolower($member['email']);
    $airmiles = $member['airmiles'];
    $voucher = $member['voucher'];
    if(!in_array($voucher, $membersCSV)) {
    print $index . ';' . $email . ';' . $voucher . ';'. "\n";

    //     	           $email = "paul@xeed.nl";

    $MailSrv = $this->getServiceLocator()->get('mail-reminder-service');
    //    	            $MailSrv->sendMail($emailInput, $emailInput, "Uw FOX Sports actiecode", $result);
    $MailSrv->sendMail($email, $email, "Vandaag is uw laatste kans om uw FOX Sports Eredivisie aankoop te activeren!", $voucher);

    //     	            die();


    $index++;
    }
    }
    die('end');
    }

    }
    */
}
