<?php

namespace Voucher\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cart
 *
 * @ORM\Table(name="cart")
 * @ORM\Entity
 */
class Cart
{
    
     const STATUS_NEW = 1;
     const STATUS_END = 2;
     const STATUS_FAIL = 4;
    /**
     * @var integer
     *
     * @ORM\Column(name="cartId", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $cartid;

    /**
     * @var integer
     *
     * @ORM\Column(name="airmiles", type="bigint", nullable=true)
     */
    private $airmiles;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;
    
    /**
     * @var string
     *
     * @ORM\Column(name="once", type="string", length=255, nullable=true)
     */
    private $once;
    

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var integer
     *
     * @ORM\Column(name="cartStatus", type="integer", nullable=true)
     */
    private $cartstatus;
    
	/**
	 * @return the $once
	 */
	public function getOnce() {
		return $this->once;
	}

	/**
	 * @param string $once
	 */
	public function setOnce($once = null) {
	    if(null == $once)
	       $once = mt_rand(1000000, 9999999);
	    
		$this->once = $once;
	}

	/**
	 * @return the $cartid
	 */
	public function getCartid() {
		return $this->cartid;
	}

	/**
	 * @return the $airmiles
	 */
	public function getAirmiles() {
		return $this->airmiles;
	}

	/**
	 * @return the $email
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * @return the $created
	 */
	public function getCreated() {
		return $this->created;
	}

	/**
	 * @return the $cartstatus
	 */
	public function getCartstatus() {
		return $this->cartstatus;
	}

	/**
	 * @param number $cartid
	 */
	public function setCartid($cartid) {
		$this->cartid = $cartid;
	}

	/**
	 * @param number $airmiles
	 */
	public function setAirmiles($airmiles) {
		$this->airmiles = $airmiles;
	}

	/**
	 * @param string $email
	 */
	public function setEmail($email) {
		$this->email = $email;
	}

	/**
	 * @param DateTime $created
	 */
	public function setCreated($created = null) {
	    if(null == $created)
	    	$created = new \DateTime(date('Y-m-d H:i:s'));
	     
		$this->created = $created;
	}

	/**
	 * @param number $cartstatus
	 */
	public function setCartstatus($cartstatus) {
		$this->cartstatus = $cartstatus;
	}



}
