<?php

namespace Voucher\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Carthistory
 *
 * @ORM\Table(name="carthistory", indexes={@ORM\Index(name="cartId", columns={"cartId"})})
 * @ORM\Entity
 */
class Carthistory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="carthistoryId", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $carthistoryid;

    /**
     * @var string
     *
     * @ORM\Column(name="soapCall", type="string", length=255, nullable=true)
     */
    private $soapcall;

    /**
     * @var integer
     *
     * @ORM\Column(name="airmilesresponse", type="integer", nullable=true)
     */
    private $airmilesresponse;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="`when`", type="datetime", nullable=true)
     */
    private $when;

    /**
     * @var \Voucher\Entity\Cart
     *
     * @ORM\ManyToOne(targetEntity="Voucher\Entity\Cart")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cartId", referencedColumnName="cartId")
     * })
     */
    private $cartid;
    
    /**
     * @var string
     *
     * @ORM\Column(name="log", type="string", length=255, nullable=true)
     */
    private $log;
    
    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=255, nullable=true)
     */
    private $ip;
    
	/**
	 * @return the $ip
	 */
	public function getIp() {
		return $this->ip;
	}

	/**
	 * @param string $ip
	 */
	public function setIp($ip = null) {
	    if(null == $ip)
	        $ip = $_SERVER["REMOTE_ADDR"];
		$this->ip = $ip;
	}

	/**
	 * @return the $log
	 */
	public function getLog() {
		return $this->log;
	}

	/**
	 * @param string $log
	 */
	public function setLog($log) {
		$this->log = $log;
	}

	/**
	 * @return the $carthistoryid
	 */
	public function getCarthistoryid() {
		return $this->carthistoryid;
	}

	/**
	 * @return the $soapcall
	 */
	public function getSoapcall() {
		return $this->soapcall;
	}

	/**
	 * @return the $airmilesresponse
	 */
	public function getAirmilesresponse() {
		return $this->airmilesresponse;
	}

	/**
	 * @return the $when
	 */
	public function getWhen() {
		return $this->when;
	}

	/**
	 * @return the $cartid
	 */
	public function getCartid() {
		return $this->cartid;
	}

	/**
	 * @param number $carthistoryid
	 */
	public function setCarthistoryid($carthistoryid) {
		$this->carthistoryid = $carthistoryid;
	}

	/**
	 * @param string $soapcall
	 */
	public function setSoapcall($soapcall) {
		$this->soapcall = $soapcall;
	}

	/**
	 * @param number $airmilesresponse
	 */
	public function setAirmilesresponse($airmilesresponse) {
		$this->airmilesresponse = $airmilesresponse;
	}

	/**
	 * @param DateTime $when
	 */
	public function setWhen($when = null) {
	    if(null == $when)
	        $when = new \DateTime(date('Y-m-d H:i:s'));
		$this->when = $when;
	}

	/**
	 * @param \Voucher\Entity\Cart $cartid
	 */
	public function setCartid($cartid) {
		$this->cartid = $cartid;
	}



}
