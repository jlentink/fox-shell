<?php

namespace Voucher\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Voucher
 *
 * @ORM\Table(name="voucher", uniqueConstraints={@ORM\UniqueConstraint(name="voucher", columns={"voucher"})}, indexes={@ORM\Index(name="cartId", columns={"cartId"})})
 * @ORM\Entity
 */
class Voucher
{
    /**
     * @var integer
     *
     * @ORM\Column(name="voucherId", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $voucherid;

    /**
     * @var string
     *
     * @ORM\Column(name="voucher", type="string", length=255, nullable=true)
     */
    private $voucher;
    
    /**
     * @var string
     *
     * @ORM\Column(name="voucherpassword", type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @var \Voucher\Entity\Cart
     *
     * @ORM\ManyToOne(targetEntity="Voucher\Entity\Cart")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cartId", referencedColumnName="cartId")
     * })
     */
    private $cartid;
    
	/**
	 * @return the $password
	 */
	public function getPassword() {
		return $this->password;
	}

	/**
	 * @param string $password
	 */
	public function setPassword($password) {
		$this->password = $password;
	}

	/**
	 * @return the $voucherid
	 */
	public function getVoucherid() {
		return $this->voucherid;
	}

	/**
	 * @return the $voucher
	 */
	public function getVoucher() {
		return $this->voucher;
	}

	/**
	 * @return the $cartid
	 */
	public function getCartid() {
		return $this->cartid;
	}



}
