<?php

namespace Voucher\Model;

class AirmilesResponse {
	
	const STATUS_OK = 0;
	const STATUS_INSUFFICIENT_FUNDS = 1;
	const STATUS_BLOCKED = 2;
	const STATUS_INFO_INCORRECT = 4;
	const STATUS_CARD_UNKOWN = 8;
	const STATUS_TECHNICAL_ERROR = 16;
	const STATUS_UNKOWN = 256;
	
	protected $_errorCode = -1;
	
	protected $_data = null;
	
	
	public function __construct($data, $errorCode) {
		$this->_data = $data;
		$this->_errorCode = $errorCode;
	}
	
	public function getData(){
		return $this->_data;
	}
	
	public function getErrorNumber(){
		return (int) $this->_errorCode;
	}
	
	public function getError(){
		switch('' . $this->_errorCode){
			case '0':
				return self::STATUS_OK;
			case '1':
				return self::STATUS_INSUFFICIENT_FUNDS;
			case '2':
				return self::STATUS_BLOCKED;
			case '6':
				return self::STATUS_INFO_INCORRECT;
			case '9':
				return self::STATUS_CARD_UNKOWN;
		}
		
		return self::STATUS_TECHNICAL_ERROR;
	}
}