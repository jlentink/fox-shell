<?php

namespace Voucher\Model\Login\ComplexType\Abstracts;

interface ComplexTypeInterface {
    protected function initComplexType();
}