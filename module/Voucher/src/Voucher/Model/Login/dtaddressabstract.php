<?php

namespace Voucher\Model\Login\Complextype\Abstracts;

abstract class DT_AddressAbstract{
	
	
	/**
	 * @var string
	 */
	 public $Street;
	 
	/**
	 * @var string
	 */
	 public $HouseNumber;
	 
	/**
	 * @var string
	 */
	 public $HouseNumberSuppliment;
	 
	/**
	 * @var string
	 */
	 public $PostalCode;
	 
	/**
	 * @var string
	 */
	 public $City;
	 
	/**
	 * @var string
	 */
	 public $Country;
	 
	/**
	 * @var string
	 */
	 public $AddressAddition;
	 
	/**
	 * @var string
	 */
	 public $AddressBounceReason;
	 
	
	abstract protected function initComplexType();

	public function __construct(){
		$this->initComplexType();
	}
}