<?php

namespace Voucher\Model\Login\Complextype\Abstracts;

abstract class DT_ContactAbstract{
	
	
	/**
	 * @var string
	 */
	 public $Email;
	 
	/**
	 * @var string
	 */
	 public $EmailBounceIndicator;
	 
	/**
	 * @var string
	 */
	 public $OptinOptoutIndicator;
	 
	/**
	 * @var string
	 */
	 public $TelephoneNrPrivate;
	 
	/**
	 * @var string
	 */
	 public $PhoneNrBounceIndicPrivate;
	 
	/**
	 * @var string
	 */
	 public $TelephoneNrMobile;
	 
	/**
	 * @var string
	 */
	 public $PhoneNrBounceIndicMobile;
	 
	
	abstract protected function initComplexType();

	public function __construct(){
		$this->initComplexType();
	}
}