<?php

namespace Voucher\Model\Login;

abstract class dtheaderabstract{


	/**
	 * @var string
	 */
	 public $MessageId = '0a3b5054-b5cc-bb62-e100-00000ad7340e';

	/**
	 * @var
	 */
	 public $MessageTime;

	/**
	 * @var string
	 */
	 public $Sender;

	/**
	 * @var string
	 */
	 public $Receiver;

	/**
	 * @var boolean
	 */
	 public $TestIndicator;


	abstract protected function initComplexType();

	public function __construct(){
		$this->initComplexType();
	}
}