<?php

namespace Voucher\Model\Login\Complextype\Abstracts;

abstract class DT_Memberinformation_NewPartnerLogon_ResponseAbstract{
	
	
	/**
	 * @var DT_Header
	 */
	 public $Header;
	 
	/**
	 * @var 
	 */
	 public $Member_Header;
	 
	
	abstract protected function initComplexType();

	public function __construct(){
		$this->initComplexType();
	}
}


/*
 stdClass::__set_state(array(
 'Header' =>
    stdClass::__set_state(array(
    'MessageId' => '0a3b5054-b5cc-bb62-e100-00000ad7340e',
    )),
 
 )),
 ))
 */