<?php

namespace Voucher\Model\Login\Complextype\Abstracts;

abstract class ExchangeFaultDataAbstract{
	
	
	/**
	 * @var string
	 */
	 public $faultText;
	 
	/**
	 * @var string
	 */
	 public $faultUrl;
	 
	/**
	 * @var ExchangeLogData
	 */
	 public $faultDetail;
	 
	
	abstract protected function initComplexType();

	public function __construct(){
		$this->initComplexType();
	}
}