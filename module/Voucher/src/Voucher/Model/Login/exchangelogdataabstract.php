<?php

namespace Voucher\Model\Login\Complextype\Abstracts;

abstract class ExchangeLogDataAbstract{
	
	
	/**
	 * @var string
	 */
	 public $severity;
	 
	/**
	 * @var string
	 */
	 public $text;
	 
	/**
	 * @var string
	 */
	 public $url;
	 
	/**
	 * @var string
	 */
	 public $id;
	 
	
	abstract protected function initComplexType();

	public function __construct(){
		$this->initComplexType();
	}
}