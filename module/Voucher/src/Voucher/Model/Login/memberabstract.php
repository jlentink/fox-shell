<?php

namespace Voucher\Model\Login;

/**
 * New Partner Logon Webservice--adding new field "Token"//Removing fields member Id & Card Number
 * @author utopia
 *
 */
abstract class memberabstract{

	/**
	 * @var string
	 */
	 public $UserID;

	/**
	 * @var string
	 */
	 public $Password;

	/**
	 * @var date
	 */
	 public $ValidDate;

	/**
	 * @var string
	 */
	 public $Partner;

	/**
	 * @var string
	 */
	 public $Token;

	abstract protected function initComplexType();

	public function __construct(){
		$this->initComplexType();
	}
}
