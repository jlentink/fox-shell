<?php

namespace Voucher\Model\Login\Complextype\Abstracts;
use Voucher\Model\Login\Complextype\Member;
use Voucher\Model\Login\Complextype\DT_Contact;

class Member_HeaderAbstract {
    /**
     * 
     * @var Member
     */
    public $Member = null;

    /**
     * 
     * @var DT_Contact
     */
    public $Contact = null;
    
    
    /**
     * 
     * @var Card
     */
    public $Card = null;
    
}

/**
 * 
/*
'Member_Header' =>
 stdClass::__set_state(array(
    'Member' =>
             stdClass::__set_state(array(
             'MemberID' => '',
             'MembershipStatus' => '',
             'Salutation' => '',
             'LastName' => '',
             'Address' =>
             stdClass::__set_state(array(
             'Street' => 'Landkaartje',
             'HouseNumber' => '213',
             'PostalCode' => '4904 ZR',
             'City' => 'OOSTERHOUT NB',
             'Country' => 'Nederland',
             )),
 'Contact' =>
         stdClass::__set_state(array(
         )),
         'Token' => 'FO03cCmKsz',
         'TTL' => '20141030114602 ',
         'UserStatus' => 'P00',
         )),
 'Card' =>
         stdClass::__set_state(array(
         'CardNumber' => '702361712',
         'CardStatus' => '',
         )),
 
 */