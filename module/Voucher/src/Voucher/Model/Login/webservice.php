<?php

namespace Voucher\Model\Login;
set_include_path(get_include_path() . PATH_SEPARATOR . dirname(__FILE__));

require_once 'mtmemberinformationnewpartnerlogonrequest.php';

class webservice{

	const MSG_STATUS_SUCCESS = 'P00';
	const MSG_STATUS_USER_UNKNOWN = 'P02';
	const MSG_STATUS_USER_PASSWORD_WRONG = 'P03';
	const MSG_STATUS_USER_NOT_ACIVATED = 'P04';
	const MSG_STATUS_USER_BLOCKED = 'P05';
	const MSG_STATUS_CARD_BLOCKED = 'P05';
	const MSG_STATUS_TOKEN_WRONG = 'P08';
	const MSG_STATUS_TOKEN_EXPIRED = 'P09';

	/**
	 * @var string
	 */
	protected $_binding = '';

	/**
	 *
	 * @var string
	 */
	protected $_username = null;

	/**
	 * @var string
	 */
	protected $_password = null;

	/**
	 * Set the eindpoint to which we are talking.
	 *
     * @param string $url
	 */
	public function setBinding($url){
		$this->_binding = $url;
	}

	public function setUsername($username){
	    $this->_username = $username;
	}

	public function setPassword($password){
	    $this->_password = $password;
	}

	public function getLoginObject(){
		return new mtmemberinformationnewpartnerlogonrequest();
	}


	/**
     * @param MT_Memberinformation_NewPartnerLogon_Request $MT_Memberinformation_NewPartnerLogon_Request
	 * @return Voucher\Model\Login\Elements\MT_Memberinformation_NewPartnerLogon_Response
	 */
	public function SIOS_Memberinformation_NewPartnerLogon(mtmemberinformationnewpartnerlogonrequest $MT_Memberinformation_NewPartnerLogon_Request){

	    $options = array( "trace" => 1, "cache_wsdl" => 0);
	    if(null !== $this->_username || null !== $this->_password) {
	        $options['login'] = $this->_username;
	        $options['password'] = $this->_password;
	    }

    		$client = new \SoapClient(dirname(__FILE__) .'/original.wsdl', $options);
		$client->__setLocation($this->_binding);
		try {
			$response = $client->SIOS_Memberinformation_NewPartnerLogon($MT_Memberinformation_NewPartnerLogon_Request);
		}

		catch(\SoapFault $fault){
			die('Soap Error:' . $fault->getMessage());
		}

		return  $response;
	}

}
