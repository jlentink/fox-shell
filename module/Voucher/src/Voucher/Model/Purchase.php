<?php

namespace Voucher\Model;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Vouchers\Model\Soap\Complextype\ProductGroup;
use Voucher\Entity\Cart;
use Voucher\Entity\Voucher;
use Voucher\Entity\Carthistory;
use Doctrine\ORM\Query\ResultSetMapping;
use Voucher\Controller\VoucherController;



class Purchase implements ServiceLocatorAwareInterface {
	
	/**
	 *
	 * @var \Zend\ServiceManager\ServiceLocatorInterface
	 */
	protected $_sm = null;
	
	/**
	 * @var \Doctrine\ORM\EntityManager 
	 */
	protected $em;
		
	/**
	 * 
	 * @var \Zend\Db\Adapter\Adapter
	 */
	protected $_adapter = null;
	
	/**
	 * 
	 * @var Voucher\Entity\Cart
	 */
	protected $_cart = null;
	
	/**
	 * 
	 * @var int
	 */
	protected $_airmiles = null;
	
	/**
	 * 
	 * @var string
	 */
	protected $_postcode = null;
	
	/**
	 * 
	 * @var int
	 */
	protected $_housenumber = null;
	
	/**
	 * 
	 * @var string
	 */
	protected $_housenumberpostfix = null;
	
	/**
	 * 
	 * @var string
	 */
	protected $_email = null;
	
	/**
	 * 
	 * @var int
	 */
	protected $_cartId = null;
	
	/**
	 * 
	 * @var double
	 */
	protected $_once = null;
	
	
	/**
	 * @return the $_once
	 */
	public function getOnce() {
		return $this->_once;
	}

	/**
	 * @param field_type $_once
	 */
	public function setOnce($once) {
		$this->_once = $once;
	}

	/**
	 * @param int $airmiles
	 */
	public function setAirmiles($airmiles) {
		$this->_airmiles = $airmiles;
	}

	/**
	 * @param string $postcode
	 */
	public function setPostcode($postcode) {
		$this->_postcode = $postcode;
	}

	/**
	 * @param int $housenumber
	 */
	public function setHousenumber($housenumber) {
		$this->_housenumber = $housenumber;
	}

	/**
	 * @param string $housenumbersuffix
	 */
	public function setHousenumberpostfix($housenumberpostfix) {
		$this->_housenumberpostfix = $housenumberpostfix;
	}

	/**
	 * @param string $email
	 */
	public function setEmail($email) {
		$this->_email = $email;
	}
	
	/**
	 * Create a cart.in the database.
	 *  
	 * @return boolean|number
	 */
	protected function createCart(){
		$em = $this->getServiceLocator()
            ->get('doctrine.entitymanager.orm_default');
        
	    try {
    	    $this->_cart = new Cart();
    	    $this->_cart->setAirmiles($this->_airmiles);
    	    $this->_cart->setEmail($this->_email);
    	    $this->_cart->setCreated();
    	    $this->_cart->setCartstatus(Cart::STATUS_NEW);
    	    $this->_cart->setOnce($this->_once);    	    
    	    
    	    $em->persist($this->_cart);
    	    $em->flush();
    	    
    	    $amHistory = new Carthistory();
    	    $amHistory->setWhen();
    	    $amHistory->setLog('Cart Created');
    	    $amHistory->setCartid($this->_cart);
    	    $amHistory->setIp();
    	    
    	    $em->persist($amHistory);
    	    $em->flush();
    	    	
    	    return true;
	    }
	    
	    catch (\Exception $e){
	        return 101;
	    }
	    
	}
	
	/**
	 * Close the cart.
	 */
	protected function finishCart(){
	    $em = $this->getServiceLocator()
	    ->get('doctrine.entitymanager.orm_default');
	    
	    try {
	        $this->_cart->setCartstatus(Cart::STATUS_END);
	         
	        $em->persist($this->_cart);
	        $em->flush();
	         
	        $amHistory = new Carthistory();
	        $amHistory->setWhen();
	        $amHistory->setLog('Cart Finished');
	        $amHistory->setCartid($this->_cart);
	        $amHistory->setIp();
	         
	        $em->persist($amHistory);
	        $em->flush();
	         
	    }
	    
	    catch (\Exception $e){
	        // nothing to do.
	    }
	}
	
	/**
	 * Return Voucher for cart
	 * 
	 * @param Cart $cart
	 */
	protected function returnVoucherForCart(Cart $cart){
	    $voucherObj = $this->getEntityManager()->getRepository('Voucher\Entity\Voucher');
	    $voucher = $voucherObj->findOneBy(array('cartid' => $cart->getCartid()));
	    
	    return $voucher->getVoucher();
	}
	
	/**
	 * Is the airmiles already used.
	 * 
	 * @return boolean
	 */
	public function isAirmilesUsed(){
	    $cartObj = $this->getEntityManager()->getRepository('Voucher\Entity\Cart');
	    $isUsed = $cartObj->findOneBy(array('airmiles' => $this->_airmiles, 'cartstatus' => Cart::STATUS_END));
	    return count($isUsed) ? true : false;	    	     
	}
	
	/**
	 * Is the once already used.
	 * 
	 * @return boolean
	 */
	public function isOnceUsed(){
	    if(!$this->_once)
	        return true;
	    
	    $cartObj = $this->getEntityManager()->getRepository('Voucher\Entity\Cart');	    
	    $isUsed = $cartObj->findOneBy(array('once' => $this->_once, 'airmiles' => $this->_airmiles));
	    
	    return count($isUsed) ? true : false;
	}
	
	/**
	 * Send email for voucher.
	 * 
	 * @param \Voucher\Entity\Voucher $voucherCode
	 */
	public function sendVoucherEmail($voucherCode){
       $MailSrv = $this->getServiceLocator()->get('mail-service');
       $MailSrv->setToEmail($this->_email);
       $MailSrv->setPassword($voucherCode->getPassword());
       $MailSrv->setVoucherCode($voucherCode->getVoucher());
       $MailSrv->send();
	}

	/**
	 * Buy a vouchercode.
	 * 
	 * @return number
	 */
	public function buy(){
  
	    $voucherObj = $this->getEntityManager()->getRepository('Voucher\Entity\Voucher');
	    $cartObj = $this->getEntityManager()->getRepository('Voucher\Entity\Cart');
//	    $vouchers = $voucherObj->findBy(array('cartid' => null));
	    

	    $vouchers = $voucherObj->findBy(array('cartid' => null), null, 10);
	    if(null === $vouchers || !count($voucherObj)){
	        return VoucherController::ERROR_OUT_OF_VOUCHERS;
	    }
	     
	     if(null !== $cartObj->findOneBy(array('once' => $this->_once, 'airmiles' => $this->_airmiles))){
	         return VoucherController::ERROR_ONCE_USED;
	     }
	     
	     $cartResult = $this->createCart();
	     if(true !== $cartResult) {
	         return VoucherController::ERROR_CANNOT_CREATE_CAR;
	     }
	     
	     // @todo: Test with airmiles
 	     $amResult = $this->handleAirmiles();
// 	     $amResult = VoucherController::ERROR_NONE;
	     if(VoucherController::ERROR_NONE != $amResult){
	         return $amResult;
	     }	     
	     	     
	     $voucherCode = $this->claimVoucher();
	     if(-1 === $voucherCode ){
             //@todo: Do Refund..
	         return VoucherController::ERROR_OUT_OF_VOUCHERS;
	     }
	     	     	     
	     $this->finishCart();
	     $this->sendVoucherEmail($voucherCode);
	     return $voucherCode->getVoucher();
	}
	
	/**
	 * Claim a voucher. if voucher is not available return -1
	 * 
	 * @return number|Ambigous <\Voucher\Entity\Voucher, NULL>
	 */
	public function claimVoucher(){
		ignore_user_abort(true);
		$con = $this->getEntityManager()->getConnection(); 
		
		$password = $this->generateRandomString();
		$rsm = new ResultSetMapping();		
		$query = $con->executeQuery('UPDATE `voucher` SET `cartId` = ?, `voucherpassword`  = ? WHERE `cartId` IS NULL LIMIT 1', array($this->_cart->getCartid(), $password));
		$voucherObj = $this->getEntityManager()->getRepository('Voucher\Entity\Voucher');
		$voucher = $voucherObj->findOneBy(array('cartid' => $this->_cart->getCartid()));
		$voucher->setPassword($password);
		
		if(null == $voucher)
		    return -1;
		
		ignore_user_abort(false);		
		return $voucher;

		
	}
	
	/**
	 * Generate a random string of lenth $length
	 * 
	 * @param int $length
	 * @return string
	 */
	protected function generateRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $randomString;
	}
	
	/**
	 * Handle airmiles saldo.
	 * 
	 * @return number
	 */
	public function handleAirmiles(){
	    $em = $this->getEntityManager();
	    $config = $this->_sm->get('config');
	    $service = $this->getServiceLocator()->get('Voucher\Model\Saldochecker');   
		$service->setAirmiles($this->_airmiles);
		$service->setPostcode($this->_postcode);
		$service->setHouseNr($this->_housenumber);

		$product = new \Voucher\Model\Soap\Complextype\ProductGroup();
		$product->airmilesPrice = $config['airmiles']['partnerdetails']['airmilesprice'];
		$product->moneyPrice    = 0;
		$product->productAmount = 1;
		$product->productCode   = $config['airmiles']['partnerdetails']['productcode'];
		
		$response = $service->CreateProductRedemptionWithPostalCode(array($product));
		
		$amHistory = new Carthistory();
		$amHistory->setAirmilesresponse($response->getErrorNumber());
		$amHistory->setSoapcall('CreateProductRedemptionWithPostalCode');
		$amHistory->setWhen();
		$amHistory->setCartid($this->_cart);
		$amHistory->setIp();		

		
		$em->persist($amHistory);
		$em->flush();		
		
		if(0 !== $response->getErrorNumber()){
		    return $response->getErrorNumber();
		}
		
		$response = $service->FinishProductRedemptionWithSecurityPostalCode($response);
		
		$amHistory = new Carthistory();
		$amHistory->setAirmilesresponse($response->getErrorNumber());
		$amHistory->setSoapcall('FinishProductRedemptionWithSecurityPostalCode');
		$amHistory->setWhen();
		$amHistory->setCartid($this->_cart);
		$amHistory->setIp();
		
		$em->persist($amHistory);
		$em->flush();
				
		return (int) $response->getErrorNumber();
		
	}
	
	/**
	 * 
	 * @return \Doctrine\ORM\EntityManager
	 */
	public function getEntityManager()
	{
		if (null === $this->em) {
			$this->em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
		}
		return $this->em;
	}	
	
	/**
	 * 
	 * @return \Zend\Db\Adapter\Adapter
	 */
	public function getAdapter(){
		
		if (!$this->_adapter) {
			$sm = $this->getServiceLocator();
			$this->_adapter = $sm->get('Zend\Db\Adapter\Adapter');
		}
		return $this->_adapter;
	}
	
	/**
	 * Mark: Service Location functions
	 */
	
	/* (non-PHPdoc)
	 * @see \Zend\ServiceManager\ServiceLocatorAwareInterface::setServiceLocator()
	*/
	public function setServiceLocator(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator) {
		$this->_sm = $serviceLocator;
	}
	
	/* (non-PHPdoc)
	 * @see \Zend\ServiceManager\ServiceLocatorAwareInterface::getServiceLocator()
	*/
	public function getServiceLocator() {
		return $this->_sm;
	}	
}
