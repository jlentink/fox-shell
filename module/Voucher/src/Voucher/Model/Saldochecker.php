<?php

namespace Voucher\Model;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Voucher\Model\Soap\webservice;
use Voucher\Model\Soap\Elements\CreateProductRedemptionWithSecurityPostalCode;
use Voucher\Model\Soap\Elements\FinishProductRedemptionWithSecurityPostalCode;
use Voucher\Entity\Carthistory;
use Voucher\Model\Soap\Elements\GetSaldoWithSecurityPostalCode;
use Voucher\Model\Soap\Complextype\SimpleDate;


class Saldochecker implements ServiceLocatorAwareInterface{
	
	/**
	 * 
	 * @var \Zend\ServiceManager\ServiceLocatorInterface
	 */
	protected $_sm = null;
	
	/**
	 * 
	 * @var string
	 */
	protected $_postcode = null;
	
	/**
	 * 
	 * @var int
	 */
	protected $_housenr = null;
	
	/**
	 * 
	 * @var int
	 */
	protected $_airmiles = null;
	
	/**
	 * @var \Doctrine\ORM\EntityManager
	 */
	protected $em;	
	
	/**
	 * 
	 * @var Product[]
	 */
	protected $_products = array();
	
	public function __construct(){
	}	
	
	/**
	 * Mark: Soap Requests
	 */	
	
	public function getSaldo(){
		$config = $this->_sm->get('config');
		$webservice = new webservice();
		$webservice->setBinding($config['airmiles']['saldochecker'][$config['airmiles']['saldochecker']['environment']]);
		
//		$GetSaldoWithSecurityPostalCode = new \Vouchers\Model\Soap\Elements\GetSaldoWithSecurityPostalCode();
		$GetSaldoWithSecurityPostalCode = new GetSaldoWithSecurityPostalCode();
		$GetSaldoWithSecurityPostalCode->aSaldoMessage->POSNumber = $config['airmiles']['partnerdetails']['posnumber'];
		$GetSaldoWithSecurityPostalCode->aSaldoMessage->brancheNumber = $config['airmiles']['partnerdetails']['branchnumber'];
		$GetSaldoWithSecurityPostalCode->aSaldoMessage->supplierCode = $config['airmiles']['partnerdetails']['suppliercode'];
		$GetSaldoWithSecurityPostalCode->aSaldoMessage->securityCode = $this->getSecurityCode();
		$GetSaldoWithSecurityPostalCode->aSaldoMessage->airmilesNumber = $this->_airmiles;
		$GetSaldoWithSecurityPostalCode->aSaldoMessage->dateTime = new SimpleDate();
		
		$GetSaldoWithSecurityPostalCode->aSaldoMessage->sequenceNumber = $this->_getSequenceNumber();
		
		$GetSaldoWithSecurityPostalCodeResponse = $webservice->GetSaldoWithSecurityPostalCode($GetSaldoWithSecurityPostalCode);
		
		$response = new AirmilesResponse($GetSaldoWithSecurityPostalCodeResponse->GetSaldoWithSecurityPostalCodeResult->saldo, $GetSaldoWithSecurityPostalCodeResponse->GetSaldoWithSecurityPostalCodeResult->resultCode);
		return $response;

	}
	
	
	public function CreateProductRedemptionWithPostalCode(array $products){
		$config = $this->_sm->get('config');
		
		$webservice = new webservice();
		$webservice->setBinding($config['airmiles']['saldochecker'][$config['airmiles']['saldochecker']['environment']]);

		$CreateProductRedemptionWithSecurityPostalCode = new CreateProductRedemptionWithSecurityPostalCode();
		$CreateProductRedemptionWithSecurityPostalCode->aProductMessage->airmilesNumber = $this->_airmiles;
		$CreateProductRedemptionWithSecurityPostalCode->aProductMessage->brancheNumber = $config['airmiles']['partnerdetails']['branchnumber'];;
		$CreateProductRedemptionWithSecurityPostalCode->aProductMessage->POSNumber = $config['airmiles']['partnerdetails']['posnumber'];
		$CreateProductRedemptionWithSecurityPostalCode->aProductMessage->securityCode = $this->getSecurityCode();
		$CreateProductRedemptionWithSecurityPostalCode->aProductMessage->sequenceNumber = $this->_getSequenceNumber();
		$CreateProductRedemptionWithSecurityPostalCode->aProductMessage->supplierCode = $config['airmiles']['partnerdetails']['suppliercode'];
		foreach($products as $product){
			$CreateProductRedemptionWithSecurityPostalCode->aProductMessage->productGroups[] = clone $product;
		}
		
		$CreateProductRedemptionWithSecurityPostalCode->aProductMessage->calculateValues();
		
		$CreateProductRedemptionWithSecurityPostalCodeResponse = $webservice->CreateProductRedemptionWithSecurityPostalCode($CreateProductRedemptionWithSecurityPostalCode);

		$data['response'] = $CreateProductRedemptionWithSecurityPostalCodeResponse->CreateProductRedemptionWithSecurityPostalCodeResult;
		$data['request'] = $CreateProductRedemptionWithSecurityPostalCode;
		$response = new AirmilesResponse($data, $CreateProductRedemptionWithSecurityPostalCodeResponse->CreateProductRedemptionWithSecurityPostalCodeResult->resultCode);		
		return $response;
	}
	
	public function  FinishProductRedemptionWithSecurityPostalCode(AirmilesResponse $finish){		
		$config = $this->_sm->get('config');
		$data = $finish->getData();
		
		$webservice = new webservice();
		$webservice->setBinding($config['airmiles']['saldochecker'][$config['airmiles']['saldochecker']['environment']]);
		
		$FinishProductRedemptionWithSecurityPostalCode = new FinishProductRedemptionWithSecurityPostalCode();
		$FinishProductRedemptionWithSecurityPostalCode->transformCreate($data['request']);
		
		$FinishProductRedemptionWithSecurityPostalCodeResponse = $webservice->FinishProductRedemptionWithSecurityPostalCode($FinishProductRedemptionWithSecurityPostalCode);
		
		$response = new AirmilesResponse($FinishProductRedemptionWithSecurityPostalCodeResponse->FinishProductRedemptionWithSecurityPostalCodeResult, $FinishProductRedemptionWithSecurityPostalCodeResponse->FinishProductRedemptionWithSecurityPostalCodeResult->resultCode);
		return $response;
	}
	

	/**
	 * Mark: Setters
	 */	
	public function setHouseNr($housenr){
		if(!ctype_digit('' . $housenr))
			throw new Exception('House number format is not a valid int', 500);
		
		$this->_housenr = $housenr;
	}
	
	public function setPostcode($postcode){
		if(!preg_match('/^[0-9]{4}[A-Z]{2}$/', $postcode))
			throw new Exception('Postcode format is incorrect', 500);

		$this->_postcode = $postcode;
	}
	
	public function getSecurityCode(){
		
		$securityCode = substr($this->_postcode, 0, 4);
		
		if(strlen('' . $this->_housenr) > 2){
			$securityCode .= substr('' . $this->_housenr, 0, 2);			
		} else if (strlen('' . $this->_housenr) < 2){
			$securityCode .= '0' . $this->_housenr;			
		} else {
			$securityCode .= '' . $this->_housenr;
		}
		
		return $securityCode;
		
	}
	
	public function setAirmiles($airmiles){
		if(!ctype_digit('' . $airmiles))
			throw new Exception('Airmiles number format is not a valid int', 500);
		
		if(strlen('' . $airmiles) != 9)
			throw new Exception('Airmiles has not a valid length', 500);
		
		
		if(!$this->_elfproef($airmiles))
			throw new Exception('Airmiles not valid occording 11 proef', 500);
		
		$this->_airmiles = $airmiles;		
	}
	
	/**
	 * Mark: Protected function
	 */	
	
	protected function _getSequenceNumber(){
		$config = $this->_sm->get('config');
		$host = $config['airmiles']['sequencenumber'];
		
		$fp = @fsockopen($host['host'], $host['port'], $errno, $errstr, 5);
		if($fp === false){
		    $amHistory = new Carthistory();		    
		    $amHistory->setSoapcall('');
		    $amHistory->setLog('Could not get sequancenumber doing mt_rand()');
		    $amHistory->setWhen(new \DateTime(date('Y-m-d H:i:s')));
		    $amHistory->setIp();
		    
		    $em = $this->getEntityManager();
		    $em->persist($amHistory);
		    $em->flush();		   
            return mt_rand(0, 99);
		}
		
		$number = fgets($fp, 128);
		fclose($fp);
		
		return $number;
	}
	
	public function addProduct(Product $product){
		$this->_products = $product;
	}
	
	protected function _elfproef($airmiles){
		$length = strlen('' . $airmiles);
		$total = 0;
		for($i=0;$i < $length;$i++)
			$total += substr($airmiles, $i, 1) * ($length - $i);
				
		return !($total % 11);
		
	}
	
	/**
	 *
	 * @return \Doctrine\ORM\EntityManager
	 */
	public function getEntityManager()
	{
		if (null === $this->em) {
			$this->em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
		}
		return $this->em;
	}	
	
	/**
	 * Mark: Service Location functions
	 */
	
	/* (non-PHPdoc)
	 * @see \Zend\ServiceManager\ServiceLocatorAwareInterface::setServiceLocator()
	 */
	public function setServiceLocator(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator) {
		$this->_sm = $serviceLocator;		
	}

	/* (non-PHPdoc)
	 * @see \Zend\ServiceManager\ServiceLocatorAwareInterface::getServiceLocator()
	 */
	public function getServiceLocator() {
		return $this->_sm;		
	}

}