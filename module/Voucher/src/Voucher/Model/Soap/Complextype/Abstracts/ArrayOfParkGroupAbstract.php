<?php

namespace Voucher\Model\Soap\Complextype\Abstracts;

abstract class ArrayOfParkGroupAbstract{
	
	
	/**
	 * @var ParkGroup
	 */
	 public $ParkGroup = null;
	 
	
	abstract protected function initComplexType();

	public function __construct(){
		$this->initComplexType();
	}
}