<?php

namespace Voucher\Model\Soap\Complextype\Abstracts;

abstract class ArrayOfProductGroupAbstract{
	
	
	/**
	 * @var ProductGroup
	 */
	 public $ProductGroup = null;
	 
	
	abstract protected function initComplexType();

	public function __construct(){
		$this->initComplexType();
	}
}