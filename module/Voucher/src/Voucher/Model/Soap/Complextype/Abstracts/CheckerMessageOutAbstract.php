<?php

namespace Voucher\Model\Soap\Complextype\Abstracts;

abstract class CheckerMessageOutAbstract{
	
	
	/**
	 * @var string
	 */
	 public $supplierCode;
	 
	/**
	 * @var integer
	 */
	 public $brancheNumber;
	 
	/**
	 * @var string
	 */
	 public $POSNumber;
	 
	/**
	 * @var SimpleDate
	 */
	 public $dateTime;
	 
	/**
	 * @var integer
	 */
	 public $airmilesNumber;
	 
	/**
	 * @var integer
	 */
	 public $securityCode;
	 
	/**
	 * @var integer
	 */
	 public $sequenceNumber;
	 
	/**
	 * @var integer
	 */
	 public $saldo;
	 
	/**
	 * @var integer
	 */
	 public $resultCode;
	 
	
	abstract protected function initComplexType();

	public function __construct(){
		$this->initComplexType();
	}
}