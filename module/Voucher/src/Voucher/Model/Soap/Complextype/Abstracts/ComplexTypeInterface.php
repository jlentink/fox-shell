<?php

namespace Voucher\Model\Soap\ComplexType\Abstracts;

interface ComplexTypeInterface {
    protected function initComplexType();
}