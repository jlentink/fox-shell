<?php

namespace Voucher\Model\Soap\Complextype\Abstracts;

abstract class ParkGroupAbstract{
	
	
	/**
	 * @var string
	 */
	 public $arrangementCode;
	 
	/**
	 * @var integer
	 */
	 public $airmilesPrice;
	 
	/**
	 * @var integer
	 */
	 public $moneyPrice;
	 
	/**
	 * @var integer
	 */
	 public $numberOfArrangements;
	 
	/**
	 * @var SimpleDate
	 */
	 public $startingDate;
	 
	/**
	 * @var boolean
	 */
	 public $insurance;
	 
	
	abstract protected function initComplexType();

	public function __construct(){
		$this->initComplexType();
	}
}