<?php

namespace Voucher\Model\Soap\Complextype\Abstracts;

abstract class ParkMessageInAbstract{
	
	
	/**
	 * @var string
	 */
	 public $supplierCode;
	 
	/**
	 * @var integer
	 */
	 public $brancheNumber;
	 
	/**
	 * @var string
	 */
	 public $POSNumber;
	 
	/**
	 * @var SimpleDate
	 */
	 public $dateTime;
	 
	/**
	 * @var integer
	 */
	 public $airmilesNumber;
	 
	/**
	 * @var integer
	 */
	 public $securityCode;
	 
	/**
	 * @var integer
	 */
	 public $sequenceNumber;
	 
	/**
	 * @var ParkGroup[]
	 */
	 public $parkGroups = array();
	 
	/**
	 * @var integer
	 */
	 public $totalAirmilesPrice;
	 
	/**
	 * @var integer
	 */
	 public $totalMoneyPrice;
	 
	/**
	 * @var string
	 */
	 public $reserveNumber;
	 
	
	abstract protected function initComplexType();

	public function __construct(){
		$this->initComplexType();
	}
}