<?php

namespace Voucher\Model\Soap\Complextype\Abstracts;

abstract class ProductGroupAbstract{
	
	
	/**
	 * @var string
	 */
	 public $productCode;
	 
	/**
	 * @var integer
	 */
	 public $airmilesPrice;
	 
	/**
	 * @var integer
	 */
	 public $moneyPrice;
	 
	/**
	 * @var integer
	 */
	 public $productAmount;
	 
	
	abstract protected function initComplexType();

	public function __construct(){
		$this->initComplexType();
	}
}