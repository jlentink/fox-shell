<?php

namespace Voucher\Model\Soap\Complextype\Abstracts;

abstract class ProductMessageInAbstract{
	
	
	/**
	 * @var string
	 */
	 public $supplierCode;
	 
	/**
	 * @var integer
	 */
	 public $brancheNumber;
	 
	/**
	 * @var string
	 */
	 public $POSNumber;
	 
	/**
	 * @var SimpleDate
	 */
	 public $dateTime;
	 
	/**
	 * @var integer
	 */
	 public $airmilesNumber;
	 
	/**
	 * @var integer
	 */
	 public $securityCode;
	 
	/**
	 * @var integer
	 */
	 public $sequenceNumber;
	 
	/**
	 * @var ProductGroup[]
	 */
	 public $productGroups = array();
	 
	/**
	 * @var integer
	 */
	 public $totalAirmilesPrice;
	 
	/**
	 * @var integer
	 */
	 public $totalMoneyPrice;
	 
	
	abstract protected function initComplexType();

	public function __construct(){
		$this->initComplexType();
	}
}