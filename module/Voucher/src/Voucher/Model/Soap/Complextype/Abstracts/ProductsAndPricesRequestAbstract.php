<?php

namespace Voucher\Model\Soap\Complextype\Abstracts;

abstract class ProductsAndPricesRequestAbstract{
	
	
	/**
	 * @var string
	 */
	 public $p_lev_code;
	 
	/**
	 * @var string
	 */
	 public $p_retour_ind;
	 
	/**
	 * @var integer
	 */
	 public $p_annulerings_termijn;
	 
	
	abstract protected function initComplexType();

	public function __construct(){
		$this->initComplexType();
	}
}