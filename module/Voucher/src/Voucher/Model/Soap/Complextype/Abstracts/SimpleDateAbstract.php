<?php

namespace Voucher\Model\Soap\Complextype\Abstracts;

abstract class SimpleDateAbstract{
	
	
	/**
	 * @var integer
	 */
	 public $year;
	 
	/**
	 * @var integer
	 */
	 public $month;
	 
	/**
	 * @var integer
	 */
	 public $day;
	 
	/**
	 * @var integer
	 */
	 public $hours;
	 
	/**
	 * @var integer
	 */
	 public $minutes;
	 
	/**
	 * @var integer
	 */
	 public $seconds;
	 
	
	abstract protected function initComplexType();

	public function __construct(){
		$this->initComplexType();
	}
}