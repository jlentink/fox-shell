<?php

namespace Voucher\Model\Soap\Complextype;
use Voucher\Model\Soap\Complextype\Abstracts\ProductMessageInAbstract;
/**
 * @todo: Implement own logic
 */ 
class ProductMessageIn extends ProductMessageInAbstract {
	public function initComplexType(){
			$this->dateTime = new SimpleDate();
			// $this->productGroups = new ArrayOfProductGroup();
	
			// Init stuff on class construction
	}
	
	public function calculateValues(){
		$this->totalAirmilesPrice = 0;
		$this->totalMoneyPrice = 0;
	
		foreach($this->productGroups as $ProductGroup){
			$this->totalAirmilesPrice += ($ProductGroup->airmilesPrice * $ProductGroup->productAmount);
			$this->totalMoneyPrice += ($ProductGroup->moneyPrice * $ProductGroup->productAmount);
		}
	}	
}