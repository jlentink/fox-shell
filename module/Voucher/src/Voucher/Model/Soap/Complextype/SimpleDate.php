<?php

namespace Voucher\Model\Soap\Complextype;
use Voucher\Model\Soap\Complextype\Abstracts\SimpleDateAbstract;
/**
 * @todo: Implement own logic
 */ 
class SimpleDate extends SimpleDateAbstract {
	public function initComplexType(){
		$this->initDate();
	}
	
	public function initDate(){
		$this->year		= date('Y');
		$this->month 	= date('n');
		$this->day 		= date('j');
		$this->hours 	= date('G');
		$this->minutes 	= date('i');
		$this->seconds 	= date('s');		
	}
}