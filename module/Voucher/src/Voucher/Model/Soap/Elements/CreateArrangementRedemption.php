<?php

namespace Voucher\Model\Soap\Elements;

use Voucher\Model\Soap\Elements\Types\aParkMessage;


class CreateArrangementRedemption {
	
	
	/**
	 * @var aParkMessage
	 */
	 public $aParkMessage;
	 
	
	public function __construct() {
		     $this->aParkMessage = new aParkMessage();

	}			
}	 
	 