<?php

namespace Voucher\Model\Soap\Elements;

use Voucher\Model\Soap\Elements\Types\CreateArrangementRedemptionResult;


class CreateArrangementRedemptionResponse {
	
	
	/**
	 * @var CreateArrangementRedemptionResult
	 */
	 public $CreateArrangementRedemptionResult;
	 
	
	public function __construct() {
		     $this->CreateArrangementRedemptionResult = new CreateArrangementRedemptionResult();

	}			
}	 
	 