<?php

namespace Voucher\Model\Soap\Elements;

use Voucher\Model\Soap\Elements\Types\CreateProductRedemptionWithSecurityDateResult;


class CreateProductRedemptionWithSecurityDateResponse {
	
	
	/**
	 * @var CreateProductRedemptionWithSecurityDateResult
	 */
	 public $CreateProductRedemptionWithSecurityDateResult;
	 
	
	public function __construct() {
		     $this->CreateProductRedemptionWithSecurityDateResult = new CreateProductRedemptionWithSecurityDateResult();

	}			
}	 
	 