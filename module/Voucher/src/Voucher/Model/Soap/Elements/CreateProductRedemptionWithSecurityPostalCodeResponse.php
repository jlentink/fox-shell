<?php

namespace Voucher\Model\Soap\Elements;

use Voucher\Model\Soap\Elements\Types\CreateProductRedemptionWithSecurityPostalCodeResult;


class CreateProductRedemptionWithSecurityPostalCodeResponse {
	
	
	/**
	 * @var CreateProductRedemptionWithSecurityPostalCodeResult
	 */
	 public $CreateProductRedemptionWithSecurityPostalCodeResult;
	 
	
	public function __construct() {
		     $this->CreateProductRedemptionWithSecurityPostalCodeResult = new CreateProductRedemptionWithSecurityPostalCodeResult();

	}			
}	 
	 