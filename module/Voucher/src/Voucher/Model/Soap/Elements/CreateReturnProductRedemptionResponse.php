<?php

namespace Voucher\Model\Soap\Elements;

use Voucher\Model\Soap\Elements\Types\CreateReturnProductRedemptionResult;


class CreateReturnProductRedemptionResponse {
	
	
	/**
	 * @var CreateReturnProductRedemptionResult
	 */
	 public $CreateReturnProductRedemptionResult;
	 
	
	public function __construct() {
		     $this->CreateReturnProductRedemptionResult = new CreateReturnProductRedemptionResult();

	}			
}	 
	 