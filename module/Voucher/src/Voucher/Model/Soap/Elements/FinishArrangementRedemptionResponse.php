<?php

namespace Voucher\Model\Soap\Elements;

use Voucher\Model\Soap\Elements\Types\FinishArrangementRedemptionResult;


class FinishArrangementRedemptionResponse {
	
	
	/**
	 * @var FinishArrangementRedemptionResult
	 */
	 public $FinishArrangementRedemptionResult;
	 
	
	public function __construct() {
		     $this->FinishArrangementRedemptionResult = new FinishArrangementRedemptionResult();

	}			
}	 
	 