<?php

namespace Voucher\Model\Soap\Elements;

use Voucher\Model\Soap\Elements\Types\FinishProductRedemptionWithSecurityDateResult;


class FinishProductRedemptionWithSecurityDateResponse {
	
	
	/**
	 * @var FinishProductRedemptionWithSecurityDateResult
	 */
	 public $FinishProductRedemptionWithSecurityDateResult;
	 
	
	public function __construct() {
		     $this->FinishProductRedemptionWithSecurityDateResult = new FinishProductRedemptionWithSecurityDateResult();

	}			
}	 
	 