<?php

namespace Voucher\Model\Soap\Elements;

use Voucher\Model\Soap\Elements\Types\aProductMessage;


class FinishProductRedemptionWithSecurityPostalCode {
	
	
	/**
	 * @var aProductMessage
	 */
	 public $aProductMessage;
	 
	
	public function __construct() {
		     $this->aProductMessage = new aProductMessage();
	}
	
	public function transformCreate($create){
		$this->aProductMessage = $create->aProductMessage;
	}
	
}	 
	 