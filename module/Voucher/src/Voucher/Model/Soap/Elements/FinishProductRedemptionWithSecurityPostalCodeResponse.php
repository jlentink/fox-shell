<?php

namespace Voucher\Model\Soap\Elements;

use Voucher\Model\Soap\Elements\Types\FinishProductRedemptionWithSecurityPostalCodeResult;


class FinishProductRedemptionWithSecurityPostalCodeResponse {
	
	
	/**
	 * @var FinishProductRedemptionWithSecurityPostalCodeResult
	 */
	 public $FinishProductRedemptionWithSecurityPostalCodeResult;
	 
	
	public function __construct() {
		     $this->FinishProductRedemptionWithSecurityPostalCodeResult = new FinishProductRedemptionWithSecurityPostalCodeResult();

	}			
}	 
	 