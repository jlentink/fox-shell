<?php

namespace Voucher\Model\Soap\Elements;

use Voucher\Model\Soap\Elements\Types\aProductMessage;


class FinishReturnProductRedemption {
	
	
	/**
	 * @var aProductMessage
	 */
	 public $aProductMessage;
	 
	
	public function __construct() {
		     $this->aProductMessage = new aProductMessage();

	}			
}	 
	 