<?php

namespace Voucher\Model\Soap\Elements;

use Voucher\Model\Soap\Elements\Types\FinishReturnProductRedemptionResult;


class FinishReturnProductRedemptionResponse {
	
	
	/**
	 * @var FinishReturnProductRedemptionResult
	 */
	 public $FinishReturnProductRedemptionResult;
	 
	
	public function __construct() {
		     $this->FinishReturnProductRedemptionResult = new FinishReturnProductRedemptionResult();

	}			
}	 
	 