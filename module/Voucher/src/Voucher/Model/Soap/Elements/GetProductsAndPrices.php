<?php

namespace Voucher\Model\Soap\Elements;

use Voucher\Model\Soap\Elements\Types\aProductsAndPricesRequest;


class GetProductsAndPrices {
	
	
	/**
	 * @var aProductsAndPricesRequest
	 */
	 public $aProductsAndPricesRequest;
	 
	
	public function __construct() {
		     $this->aProductsAndPricesRequest = new aProductsAndPricesRequest();

	}			
}	 
	 