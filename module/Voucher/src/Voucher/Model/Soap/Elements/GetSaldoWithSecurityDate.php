<?php

namespace Voucher\Model\Soap\Elements;

use Voucher\Model\Soap\Elements\Types\aSaldoMessage;


class GetSaldoWithSecurityDate {
	
	
	/**
	 * @var aSaldoMessage
	 */
	 public $aSaldoMessage;
	 
	
	public function __construct() {
		     $this->aSaldoMessage = new aSaldoMessage();

	}			
}	 
	 