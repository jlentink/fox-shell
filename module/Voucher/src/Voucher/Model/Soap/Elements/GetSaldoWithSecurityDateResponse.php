<?php

namespace Voucher\Model\Soap\Elements;

use Voucher\Model\Soap\Elements\Types\GetSaldoWithSecurityDateResult;


class GetSaldoWithSecurityDateResponse {
	
	
	/**
	 * @var GetSaldoWithSecurityDateResult
	 */
	 public $GetSaldoWithSecurityDateResult;
	 
	
	public function __construct() {
		     $this->GetSaldoWithSecurityDateResult = new GetSaldoWithSecurityDateResult();

	}			
}	 
	 