<?php

namespace Voucher\Model\Soap\Elements;

use Voucher\Model\Soap\Elements\Types\GetSaldoWithSecurityPostalCodeResult;


class GetSaldoWithSecurityPostalCodeResponse {
	
	
	/**
	 * @var GetSaldoWithSecurityPostalCodeResult
	 */
	 public $GetSaldoWithSecurityPostalCodeResult;
	 
	
	public function __construct() {
		     $this->GetSaldoWithSecurityPostalCodeResult = new GetSaldoWithSecurityPostalCodeResult();

	}			
}	 
	 