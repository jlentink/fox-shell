<?php

namespace Voucher\Model\Soap;

class SoapClient  extends \SoapClient {
	
	protected $_debug = true;
	
	function __doRequest($request, $location, $action, $version, $one_way = 0) {
		$response = parent::__doRequest($request, $location, $action, $version, $one_way);		
		if($this->_debug){
			$filename = tempnam('/tmp', 'debug__airmiles');
			file_put_contents($filename, $location. "\n" . $action . "\n" .$version . "\n" .$one_way . "\n" ."----------------------\n\n" .$request ."----------------------\n\n" . $response, FILE_APPEND);
		}
		
		return $response;
	}
	
}