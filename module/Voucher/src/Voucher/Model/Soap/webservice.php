<?php

namespace Voucher\Model\Soap;

use Voucher\Model\Soap\Elements\CreateProductRedemptionWithSecurityDate;
use Voucher\Model\Soap\Elements\CreateProductRedemptionWithSecurityDateResponse;
use Voucher\Model\Soap\Elements\CreateArrangementRedemption;
use Voucher\Model\Soap\Elements\CreateArrangementRedemptionResponse;
use Voucher\Model\Soap\Elements\FinishReturnProductRedemption;
use Voucher\Model\Soap\Elements\FinishReturnProductRedemptionResponse;
use Voucher\Model\Soap\Elements\GetSaldoWithSecurityPostalCode;
use Voucher\Model\Soap\Elements\GetSaldoWithSecurityPostalCodeResponse;
use Voucher\Model\Soap\Elements\FinishArrangementRedemption;
use Voucher\Model\Soap\Elements\FinishArrangementRedemptionResponse;
use Voucher\Model\Soap\Elements\GetSaldoWithSecurityDate;
use Voucher\Model\Soap\Elements\GetSaldoWithSecurityDateResponse;
use Voucher\Model\Soap\Elements\FinishProductRedemptionWithSecurityDate;
use Voucher\Model\Soap\Elements\FinishProductRedemptionWithSecurityDateResponse;
use Voucher\Model\Soap\Elements\CreateProductRedemptionWithSecurityPostalCode;
use Voucher\Model\Soap\Elements\CreateProductRedemptionWithSecurityPostalCodeResponse;
use Voucher\Model\Soap\Elements\GetProductsAndPrices;
use Voucher\Model\Soap\Elements\GetProductsAndPricesResponse;
use Voucher\Model\Soap\Elements\FinishProductRedemptionWithSecurityPostalCode;
use Voucher\Model\Soap\Elements\FinishProductRedemptionWithSecurityPostalCodeResponse;
use Voucher\Model\Soap\Elements\HelloWorld;
use Voucher\Model\Soap\Elements\HelloWorldResponse;
use Voucher\Model\Soap\Elements\CreateReturnProductRedemption;
use Voucher\Model\Soap\Elements\CreateReturnProductRedemptionResponse;


class webservice{

	/**
	 * @var string
	 */
	protected $_binding = '';
	
	/**
	 * Set the eindpoint to which we are talking.
	 *
     * @param string $url
	 */
	public function setBinding($url){
		$this->_binding = $url;
	}
	
	
	/**
     * @param CreateProductRedemptionWithSecurityDate $CreateProductRedemptionWithSecurityDate
	 * @return Voucher\Model\Soap\Elements\CreateProductRedemptionWithSecurityDateResponse
	 */	
	public function CreateProductRedemptionWithSecurityDate(CreateProductRedemptionWithSecurityDate $CreateProductRedemptionWithSecurityDate){
		$client = new SoapClient(dirname(__FILE__) .'/original.wsdl', array( "trace" => 1, "cache_wsdl" => 0));
		$client->__setLocation($this->_binding);
		return $client->CreateProductRedemptionWithSecurityDate($CreateProductRedemptionWithSecurityDate);	
	}

	/**
     * @param CreateArrangementRedemption $CreateArrangementRedemption
	 * @return Voucher\Model\Soap\Elements\CreateArrangementRedemptionResponse
	 */	
	public function CreateArrangementRedemption(CreateArrangementRedemption $CreateArrangementRedemption){
		$client = new SoapClient(dirname(__FILE__) .'/original.wsdl', array( "trace" => 1, "cache_wsdl" => 0));
		$client->__setLocation($this->_binding);
		return $client->CreateArrangementRedemption($CreateArrangementRedemption);	
	}

	/**
     * @param FinishReturnProductRedemption $FinishReturnProductRedemption
	 * @return Voucher\Model\Soap\Elements\FinishReturnProductRedemptionResponse
	 */	
	public function FinishReturnProductRedemption(FinishReturnProductRedemption $FinishReturnProductRedemption){
		$client = new SoapClient(dirname(__FILE__) .'/original.wsdl', array( "trace" => 1, "cache_wsdl" => 0));
		$client->__setLocation($this->_binding);
		return $client->FinishReturnProductRedemption($FinishReturnProductRedemption);	
	}

	/**
     * @param GetSaldoWithSecurityPostalCode $GetSaldoWithSecurityPostalCode
	 * @return Voucher\Model\Soap\Elements\GetSaldoWithSecurityPostalCodeResponse
	 */	
	public function GetSaldoWithSecurityPostalCode(GetSaldoWithSecurityPostalCode $GetSaldoWithSecurityPostalCode){
		$client = new SoapClient(dirname(__FILE__) .'/original.wsdl', array( "trace" => 1, "cache_wsdl" => 0));
		$client->__setLocation($this->_binding);
		return $client->GetSaldoWithSecurityPostalCode($GetSaldoWithSecurityPostalCode);	
	}

	/**
     * @param FinishArrangementRedemption $FinishArrangementRedemption
	 * @return Voucher\Model\Soap\Elements\FinishArrangementRedemptionResponse
	 */	
	public function FinishArrangementRedemption(FinishArrangementRedemption $FinishArrangementRedemption){
		$client = new SoapClient(dirname(__FILE__) .'/original.wsdl', array( "trace" => 1, "cache_wsdl" => 0));
		$client->__setLocation($this->_binding);
		return $client->FinishArrangementRedemption($FinishArrangementRedemption);	
	}

	/**
     * @param GetSaldoWithSecurityDate $GetSaldoWithSecurityDate
	 * @return Voucher\Model\Soap\Elements\GetSaldoWithSecurityDateResponse
	 */	
	public function GetSaldoWithSecurityDate(GetSaldoWithSecurityDate $GetSaldoWithSecurityDate){
		$client = new SoapClient(dirname(__FILE__) .'/original.wsdl', array( "trace" => 1, "cache_wsdl" => 0));
		$client->__setLocation($this->_binding);
		return $client->GetSaldoWithSecurityDate($GetSaldoWithSecurityDate);	
	}

	/**
     * @param FinishProductRedemptionWithSecurityDate $FinishProductRedemptionWithSecurityDate
	 * @return Voucher\Model\Soap\Elements\FinishProductRedemptionWithSecurityDateResponse
	 */	
	public function FinishProductRedemptionWithSecurityDate(FinishProductRedemptionWithSecurityDate $FinishProductRedemptionWithSecurityDate){
		$client = new SoapClient(dirname(__FILE__) .'/original.wsdl', array( "trace" => 1, "cache_wsdl" => 0));
		$client->__setLocation($this->_binding);
		return $client->FinishProductRedemptionWithSecurityDate($FinishProductRedemptionWithSecurityDate);	
	}

	/**
     * @param CreateProductRedemptionWithSecurityPostalCode $CreateProductRedemptionWithSecurityPostalCode
	 * @return Voucher\Model\Soap\Elements\CreateProductRedemptionWithSecurityPostalCodeResponse
	 */	
	public function CreateProductRedemptionWithSecurityPostalCode(CreateProductRedemptionWithSecurityPostalCode $CreateProductRedemptionWithSecurityPostalCode){
		$client = new SoapClient(dirname(__FILE__) .'/original.wsdl', array( "trace" => 1, "cache_wsdl" => 0));	
		$client->__setLocation($this->_binding);
		return $client->CreateProductRedemptionWithSecurityPostalCode($CreateProductRedemptionWithSecurityPostalCode);	
	}

	/**
     * @param GetProductsAndPrices $GetProductsAndPrices
	 * @return Voucher\Model\Soap\Elements\GetProductsAndPricesResponse
	 */	
	public function GetProductsAndPrices(GetProductsAndPrices $GetProductsAndPrices){
		$client = new SoapClient(dirname(__FILE__) .'/original.wsdl', array( "trace" => 1, "cache_wsdl" => 0));
		$client->__setLocation($this->_binding);
		return $client->GetProductsAndPrices($GetProductsAndPrices);	
	}

	/**
     * @param FinishProductRedemptionWithSecurityPostalCode $FinishProductRedemptionWithSecurityPostalCode
	 * @return Voucher\Model\Soap\Elements\FinishProductRedemptionWithSecurityPostalCodeResponse
	 */	
	public function FinishProductRedemptionWithSecurityPostalCode(FinishProductRedemptionWithSecurityPostalCode $FinishProductRedemptionWithSecurityPostalCode){
		$client = new SoapClient(dirname(__FILE__) .'/original.wsdl', array( "trace" => 1, "cache_wsdl" => 0));
		$client->__setLocation($this->_binding);
		return $client->FinishProductRedemptionWithSecurityPostalCode($FinishProductRedemptionWithSecurityPostalCode);	
	}

	/**
     * @param HelloWorld $HelloWorld
	 * @return Voucher\Model\Soap\Elements\HelloWorldResponse
	 */	
	public function HelloWorld(HelloWorld $HelloWorld){
		$client = new SoapClient(dirname(__FILE__) .'/original.wsdl', array( "trace" => 1, "cache_wsdl" => 0));
		$client->__setLocation($this->_binding);
		return $client->HelloWorld($HelloWorld);	
	}

	/**
     * @param CreateReturnProductRedemption $CreateReturnProductRedemption
	 * @return Voucher\Model\Soap\Elements\CreateReturnProductRedemptionResponse
	 */	
	public function CreateReturnProductRedemption(CreateReturnProductRedemption $CreateReturnProductRedemption){
		$client = new SoapClient(dirname(__FILE__) .'/original.wsdl', array( "trace" => 1, "cache_wsdl" => 0));
		$client->__setLocation($this->_binding);
		return $client->CreateReturnProductRedemption($CreateReturnProductRedemption);	
	}

}