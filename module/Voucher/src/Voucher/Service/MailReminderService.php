<?php

namespace Voucher\Service;

use Zend\Mail;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;

class MailReminderService {

    /**
     * 
     * @var string
     */
    protected $_basePath;
    
    /**
     * 
     * @var string
     */
    protected $_toEmail = '';
    
    /**
     * 
     * @var string
     */
    protected $_password = '';
    
    /**
     * 
     * @var String
     */
    protected $_voucherCode = '';
    
    /**
     * @var ServiceLocatorInterface 
     */
    protected $serviceLocator;
    
    
    /**
	 * @return the $_password
	 */
	public function getPassword() {
		return $this->_password;
	}

	/**
	 * @param string $_password
	 */
	public function setPassword($password) {
		$this->_password = $password;
	}

	/**
	 * @param \Voucher\Service\ServiceLocatorInterface $serviceLocator
	 */
	public function setServiceLocator($serviceLocator) {
		$this->serviceLocator = $serviceLocator;
	}
	
	/**
	 * 
	 * @return \Voucher\Service\ServiceLocatorInterface
	 */
	public function getServiceLocator() {
		return $this->serviceLocator;
	}

	/**
     * @return the $_toEmail
     */
    public function getToEmail() {
    	return $this->_toEmail;
    }
    
    /**
     * @return the $_voucherCode
     */
    public function getVoucherCode() {
    	return $this->_voucherCode;
    }
    
    /**
     * @param string $_toEmail
     */
    public function setToEmail($_toEmail) {
    	$this->_toEmail = $_toEmail;
    }
    
    /**
     * @param string $_voucherCode
     */
    public function setVoucherCode($_voucherCode) {
    	$this->_voucherCode = $_voucherCode;
    }
    
    public function substituteTags($text, $online){
        $text = str_replace('{{basepath}}', $this->_basePath, $text);
        $text = str_replace('{{voucher}}', $this->_voucherCode, $text);
        $text = str_replace('{{password}}', $this->_password, $text);
        if(!$online){
            $text = str_replace('{{ONLINE_LINK}}', '', $text);
            $text = str_replace('{{/ONLINE_LINK}}', '', $text);
            
        }else {
            $text = preg_replace('/{{ONLINE_LINK}}(.*){{\/ONLINE_LINK}}/', '', $text);
        }
        return $text;
    }
    
    
    public function send()
    {
        
        $config = $this->getServiceLocator()->get('Config');
             
        $htmlBody = $this->generateHTML();
        $textBody = $this->generateText();
        
        $htmlPart = new MimePart($htmlBody);
        $htmlPart->type = "text/html";

        $textPart = new MimePart($textBody);
        $textPart->type = "text/plain";
        
        $body = new MimeMessage();
        $body->setParts(array($textPart, $htmlPart));
        
        $message = new Mail\Message();
        $message->setFrom($config['campaign']['email']['from']['email'], $config['campaign']['email']['from']['name']);
        $message->setReplyTo($config['campaign']['email']['from']['reply-to']);
        $message->addTo($this->_toEmail);
        $message->setSubject('Herinnering: Vergeet niet jouw HBO code vandaag nog te activeren.');        
        $message->setEncoding("UTF-8");
        $message->setBody($body);
        $message->getHeaders()->get('content-type')->setType('multipart/alternative');
        
        
        $transport = new Mail\Transport\Smtp();
        $options   = new Mail\Transport\SmtpOptions(array(
        		'name'              => 'xeed',
        		'host'              => $config['campaign']['email']['server']['hostname'],
        		'port'              => $config['campaign']['email']['server']['port']
        ));
        $transport->setOptions($options);
        $transport->send($message);
    }    
    
    /**
	 * @return the $_basePath
	 */
	public function getBasePath() {
		return $this->_basePath;
	}

	/**
	 * 
	 * @param boolean $online
	 * @return string
	 */
	public function generateText($online = false){
	    $config = $this->getServiceLocator()->get('Config');
	    $textBody = file_get_contents(getcwd() . '/data/mailtemplates/'. $config['campaign']['email']['template'] . '/email-reminder.txt');
	    $textBody = $this->substituteTags($textBody, $online);
	    return $textBody;	    
	}
	
	/**
	 * 
	 * @param boolean $online
	 * @return string
	 */
	public function generateHTML($online = false){
	    $config = $this->getServiceLocator()->get('Config');	
	    $htmlBody = file_get_contents(getcwd() . '/data/mailtemplates/'. $config['campaign']['email']['template'] . '/email-reminder.html');
	    $htmlBody = $this->substituteTags($htmlBody, $online);
	    
	    return $htmlBody;
    }

    public function setBasePath($path)
    {
    	$this->_basePath = $path;
    }
}
