<?php
namespace Voucher\Service;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;


class MailServiceFactory implements FactoryInterface
{
    /**
     * (non-PHPdoc)
     * @return \Voucher\Service\MailService
     */
	public function createService(ServiceLocatorInterface $serviceLocator)
	{
		$service = new MailService();
		$request =$serviceLocator->get('Request');
		$uri = $request->getUri();
		//$uri = $this->getRequest()->getUri();
		$scheme = $uri->getScheme();
		$host = $uri->getHost();
		$host = 'www.my-shirt.nl';
		$port = $uri->getPort();
		$base = sprintf('%s://%s:%s%s', $scheme, $host,$port, $request->getBasePath());
		$path = $base;

		$service->setBasePath($path);
        $service->setServiceLocator($serviceLocator);

		return $service;
	}


}