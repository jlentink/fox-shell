function checkHousenumber(){
	//Check housenumber
	$('input[name="housenumber"]').val($('input[name="housenumber"]').val().replace(/\s/g, ''));
	if ($('input[name="housenumber"]').val() === ''){
		$('input[name="housenumber"]').addClass('error');
		return false;
	} else {
		var housenumber = $('input[name="housenumber"]').val();
		var rege2 = /^([0-9]*)$/;
		if (!rege2.test($('input[name="housenumber"]').val())){
			$('input[name="housenumber"]').addClass('error');
			return false;
		} else {
			$('input[name="housenumber"]').removeClass('error');
			return true;
		}
	}

}

function checkPostcode(){
	//Check zipcode
	$('input[name="zipcode"]').val($('input[name="zipcode"]').val().toUpperCase());
	$('input[name="zipcode"]').val($('input[name="zipcode"]').val().replace(/\s/g, ''));
	if ($('input[name="zipcode"]').val() === ''){
		$('input[name="zipcode"]').addClass('error');
		return false;
	} else {
		var rege = /^\d{4}[A-Z]{2}$/;
		if (!rege.test($('input[name="zipcode"]').val())){
			$('input[name="zipcode"]').addClass('error');
			return false;
		} else {
			$('input[name="zipcode"]').removeClass('error');
			return true;
		}
	}

}

function checkEmailTwice(){
	if ($('input[name="email"]').val() != $('input[name="email_check"]').val() || $('input[name="email_check"]').val() === ''){
		$('input[name="email_check"]').addClass('error');
		return false;
	}else {
		$('input[name="email_check"]').removeClass('error');
		return true;
	}

}

function checkEmail(){
	if ($('input[name="email"]').val() === ''){
		$('input[name="email"]').addClass('error');
		return false;
	} else {
		if($('input[name="email"]').val().indexOf('@') === -1 || $('input[name="email"]').val().indexOf('.') === -1){
			$('input[name="email"]').addClass('error');
			return false;
		} else{
			$('input[name="email"]').removeClass('error');
			return true;
		}
	}
}

function checkAirmiles(){
	//Check cartnumber
	if ($('input[name="airmilesnumber"]').val().length < 9 || $('input[name="airmilesnumber"]').val().length > 9){
		$('input[name="airmilesnumber"]').addClass('error');
		return false;
	} else {
		if(!elevenCheck($('input[name="airmilesnumber"]').val())){
			$('input[name="airmilesnumber"]').addClass('error');
			return false;
		} else {
			$('input[name="airmilesnumber"]').removeClass('error');
			return true;
		}
	}
}

function elevenCheck(am) {
	var filter = /^[0-9]{9}$/;
	if(!filter.test(am))
		return false;



    var amstr = am.toString();
    var amlength = amstr.length;
    var t = 0;
    for(var i=0;i < amlength;i++)
            t+= amstr.substr(i, 1) * (amlength - i);
    var bReturn = false;
    bReturn = (t % 11);
    return !bReturn;
}