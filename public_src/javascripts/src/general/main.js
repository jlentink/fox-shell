function onresize(){

	$('.tmpFlexSlider').remove();
	if($( document ).width() <= 900){
		$('.flexslider2').hide();
		$newElement = $('.flexslider2').clone();
		$newElement.addClass('tmpFlexSlider');
		$newElement.removeClass('flexslider2');
		$newElement.show();
		$newElement.insertBefore($('.flexslider2'));

		$('.tmpFlexSlider').flexslider({
			animation: "slide",
			animationLoop: false,
			itemWidth: $( document ).width(),
			itemMargin: 1,
			touch: true,
			slideshow: false
		});
	} else {
		$('.flexslider2').show();
	}

	resizeShadow();
}

$(function() {
	if(typeof currentError != 'undefined' && parseInt(currentError) > 0) {
		if(currentError == 9000 || currentError == 105){
			if(currentError == 105){
				$('a.pay').hide();
				$('input[name=email]').attr('readonly', true);
			}
			$('.airmilesContainer').text(currentAm);
			$('.airmilesSaldoContainer').text(currentSaldo);
			$('div.login').hide();
			$('div.pay').show();


		}
		showError(currentError);
	}
	$('.flexslider1').flexslider({
		animation: "slide",
		animationLoop: false,
		itemWidth: 150,
		itemMargin: 2,
		touch: true,
		slideshow: false
	});

	$('.flexslider3').flexslider({
		animation: "slide",
		animationLoop: false,
		itemWidth: 200,
		itemMargin: 1,
		touch: true,
		slideshow: false
	});

	onresize();
	$( window ).resize(function(){onresize();});

	$('div.faq > ul > li').click(function(){
		if($( document ).width() <= 900){
			if($('.expanded:visible', $(this)).length){
				$('.expanded', $(this)).hide();
			} else {
				$('.expanded', $(this)).show();
			}

		} else {
			showPopover($('.expanded', $(this)).html());
		}
	});

	$('.close').click(hidePopover);

	$(document).keyup(function(e) {
		if (e.keyCode == 27 && $('div#popover').is(":visible")) {
			hidePopover();
		}
	});

	$('.form-button.login').click(function(){

		if(validateFieldsLogin()){
			showSpinner();
			$.post(basePath + '/login-am', {username: $('input[name=username]').val(), password: $('input[name=password]').val()}, function(data){
				hideSpinner();
				if(!data.result) {
					showError(data.data.error);
				}else {
					$('.airmilesContainer').text(data.data.amnr);
					$('.airmilesSaldoContainer').text(data.data.saldo);

					$('div.login').hide();
					$('div.pay').show();
//					$('input[name=once]').val(data.data.once);

					if(data.data.saldo < 80){
						$('.popup-insufficient-funds').fadeIn();
						$('a.form-button.pay').hide();
					}
				}

			});
		}

	});
		$('.form-button.pay').click(function(){
			if($('.airmilesSaldoContainer').text() >= 80 ){
				if(checkEmail()){
					showSpinner();
					$('#payForm').submit();
					console.log($('#payForm').serialize());
					$('input[name=email]').parent().removeClass('has-error');
				} else {
					$('input[name=email]').parent().addClass('has-error');
				}
			}
		});
});

function showPopover(content){
	dropShadow();
	if(content)
		$('#popover > div.content').html(content);


	$('#popover').show();
}


function hidePopover(){
	liftShadow();
	$('#popover').hide();
}

function resizeShadow(force){
	if(force || $('#shadow').is(":visible")){
		$width  = $( window ).width();
		$height = $( window ).height();


		$("#shadow").css("width", $width);
		$("#shadow").css("height", $height);
	}

}

function forceBuy(url){
	showSpinner();
	setTimeout(function(){
		window.location.href=url;
	}, 100);

}

function dropShadow(){
	resizeShadow(true);
	$('#shadow').show();
}

function liftShadow(){
	$('#shadow').fadeOut();
}


function validateFieldsLogin(){
	var bError = false;
	if(!$('input[name=username]').val().length){
		bError = true;
		$('input[name=username]').parent().addClass('has-error');
	} else {
		$('input[name=username]').parent().removeClass('has-error');
	}

	if(!$('input[name=password]').val().length){
		bError = true;
		$('input[name=password]').parent().addClass('has-error');
	} else {
		$('input[name=password]').parent().removeClass('has-error');
	}

	//decide action
	if (bError && showError){
		showError(99);
		/*
		$('.popup99').fadeIn();
		$('.popup-background').fadeIn();
		$('html, body').animate({
		        scrollTop: $('.popup99').offset().top - 10
		}, 500);
		*/
	}

	return !bError;
}

function showError(errorNr){
	if(!$('.popup' + errorNr).length)
		errorNr = 999;

	$errorMsg = $('.popup' + errorNr).clone();
	$errorMsg.insertBefore($('.popup' + errorNr));
	$errorMsg.fadeIn();

	$('html, body').animate({
	        scrollTop: $('.popup' + errorNr).offset().top - 10
	}, 500);
}

function hideSpinner(){
	liftShadow();
	$('#spinner-container').empty();
	$('#spinner-text').hide();
}
function showSpinner(){
	dropShadow();
	$('#spinner-text').show();
	var spinner = new Spinner({
		lines: 15, // The number of lines to draw
		length: 26, // The length of each line
		width: 10, // The line thickness
		radius: 33, // The radius of the inner circle
		corners: 1, // Corner roundness (0..1)
		rotate: 0, // The rotation offset
		direction: 1, // 1: clockwise, -1: counterclockwise
		color: '#FFF', // #rgb or #rrggbb or array of colors
		speed: 1, // Rounds per second
		trail: 50, // Afterglow percentage
		shadow: false, // Whether to render a shadow
		hwaccel: false, // Whether to use hardware acceleration
		className: 'spinner', // The CSS class to assign to the spinner
		zIndex: 2e9, // The z-index (defaults to 2000000000)
		top: '50%', // Top position relative to parent
		left: '50%' // Left position relative to parent
	}).spin(document.getElementById('spinner-container'));
	return true;
}


/*

var submitting = false;

	$(document).keyup(function(e) {
		if (e.keyCode == 27 && $('section#dialog').is(":visible")) {
			closePopup();
		}
		if (e.keyCode == 27 && $('section#trailerDesktop').is(":visible")) {
			closeTrailer();
		}
	});

	$('#OpenTrailer').click(function(){
		if ($('div.isDesktop').is(":visible")) {
			openTrailer();
		} else {
			document.location = '/hbo/video/The Knick seizoen 1 - teaser.mp4';
		}
	});

	$('#closeTrailer').click(function(){
		closeTrailer();
	});

	$('.faq-open').click(function(){
//		$holder = $(this).parent().parent().parent();
		$holder = $(this).closest('li');
		if($('.isMobile').is(":visible") ){
			$('article', $holder).show();
			$('.read-more-container', $holder).hide();
			$('.read-less-container', $holder).show();
			$('html, body').animate({
			        scrollTop: $holder.offset().top
			}, 500);
		} else {
			dropShadow();
			$('section#dialog > h4').html($('h4', $holder).html());
			$('section#dialog > article').html($('article', $holder).html());
			$('section#dialog').show();
		}

	});

	$('.faq-close').click(function(){
		$holder = $(this).closest('li');
		if($('.isMobile').is(":visible") ){
			$('article', $holder).hide();
			$('.read-more-container', $holder).show();
			$('.read-less-container', $holder).hide();
			$('html, body').animate({
			        scrollTop: $holder.offset().top
			}, 500);
		}
	});

	$('.sprite-close').click(closePopup);


	if($('.isMobile').is(":visible") ){
		$('.thatswhy > ul').carouFredSel({
			circular: true,
			infinite: true,

			pagination: {
				container: '.thatswhy-pagination',
				anchorBuilder: function(nr){
					return '<a href="#'+nr+'"><span><i class="fa fa-circle-o"></i></span></a>';
				}
			},
			responsive: true,
			height: '300px',
			scroll : {
				fx				: "crossfade",
				items           : 1,
				duration        : 1000,
				pauseOnHover    : true,
				onAfter			: onWhyChange
			},
			swipe: {
				onTouch: true
			},
			auto: {
				play	: false
			}
		});
		onWhyChange();
	}

	if($('.isMobile').is(":visible") ){
		$('.dezezomer > ul').carouFredSel({
			circular: true,
			infinite: true,

			pagination: {
				container: '.dezezomer-pagination',
				anchorBuilder: function(nr){
					return '<a href="#'+nr+'"><span><i class="fa fa-circle-o"></i></span></a>';
				}
			},
			responsive: true,
			height: '300px',
			scroll : {
				fx				: "crossfade",
				items           : 1,
				duration        : 1000,
				pauseOnHover    : true,
				onAfter			: onZomerChange
			},
			swipe: {
				onTouch: true
			},
			auto: {
				play	: false
			}
		});
		onZomerChange();
	}
	//show popup
	$('.popup-link').click(function(e){
		e.preventDefault();
		var popup = $(this).data('popup');
		$('.'+popup).fadeIn();
		$('.popup-background').fadeIn();

	});

	//close popup
	$('.popup-close').click(function(e){
		e.preventDefault();
		$(this).parent().fadeOut();
		$('.popup-background').fadeOut();

	});


	//Cart validation and submit button
	$('.cart-confirm-button').click(function(){
		if(!submitting){
			submitting = true;
			var status = validateFields(true);
			if(status) {
				$('.button cart-confirm-button').hide();
				showSpinner();
//				$('#cart').submit();
				setTimeout(function(){$('#cart').submit();}, 100);
			}
			submitting = false;
			return false;

		} else {
			return false;
		}



	});

	$('input[name="airmilesnumber"]').change(function(){
		checkAirmiles();
	});

	$('input[name="housenumber"]').change(function(){
		checkHousenumber();
	});

	$('input[name="email"]').change(function(){
		checkEmail();
		checkEmailTwice();
	});

	$('input[name="email_check"]').change(function(){
		checkEmailTwice();
		checkEmail();
	});

	$('input[name="zipcode"]').change(function(){
		checkPostcode();
	});

	$( window ).resize(function(){resizeShadow(false);});
});

function onWhyChange(){
	$.each($('.thatswhy-pagination > a > span > i'), function(index, item){
			$(item).removeClass('fa-dot-circle-o');
			$(item).removeClass('fa-circle-o');
			$(item).addClass('fa-circle-o');
	});

	$.each($('.thatswhy-pagination > a.selected > span > i'), function(index, item){
			$(item).removeClass('fa-circle-o');
			$(item).addClass('fa-dot-circle-o');

	});
}

function onZomerChange(){
	$.each($('.dezezomer-pagination > a > span > i'), function(index, item){
			$(item).removeClass('fa-dot-circle-o');
			$(item).removeClass('fa-circle-o');
			$(item).addClass('fa-circle-o');
	});

	$.each($('.dezezomer-pagination > a.selected > span > i'), function(index, item){
			$(item).removeClass('fa-circle-o');
			$(item).addClass('fa-dot-circle-o');

	});
}

function closePopup(){
	liftShadow();
	$('#dialog').hide();
}

function openTrailer(){
	dropShadow();
	$('#trailerDesktop').show();
}
function closeTrailer(){
	 videojs('example_video_1').pause();
	liftShadow();
	$('#trailerDesktop').fadeOut();
}

function resizeShadow(force){
	if(force || $('#shadow').is(":visible")){
		$width  = $( window ).width();
		$height = $( window ).height();

		$("#shadow").css("width", $width);
		$("#shadow").css("height", $height);
	}

}

function dropShadow(){
	resizeShadow(true);
	$('#shadow').show();
}

function liftShadow(){
	$('#shadow').fadeOut();
}


function showSpinner(){
	dropShadow();
	$('#spinner-text').show();
	var spinner = new Spinner({
		lines: 15, // The number of lines to draw
		length: 26, // The length of each line
		width: 10, // The line thickness
		radius: 33, // The radius of the inner circle
		corners: 1, // Corner roundness (0..1)
		rotate: 0, // The rotation offset
		direction: 1, // 1: clockwise, -1: counterclockwise
		color: '#FFF', // #rgb or #rrggbb or array of colors
		speed: 1, // Rounds per second
		trail: 50, // Afterglow percentage
		shadow: false, // Whether to render a shadow
		hwaccel: false, // Whether to use hardware acceleration
		className: 'spinner', // The CSS class to assign to the spinner
		zIndex: 2e9, // The z-index (defaults to 2000000000)
		top: '50%', // Top position relative to parent
		left: '50%' // Left position relative to parent
	}).spin(document.getElementById('spinner-container'));
	return true;
}

function showError(errorNr){
			validateFields(false);

			if(!$('.popup' + errorNr).length)
				errorNr = 999;

			$('.popup' + errorNr).show();

			$('html, body').animate({
			        scrollTop: $('.popup' + errorNr).offset().top - 10
			}, 500);


}

function forceBuy(url){
	showSpinner();
	setTimeout(function(){
		window.location.href=url;
	}, 100);

}

function validateFields(showError){
	var bError=false;

	if(!checkAirmiles())
		bError = true;

	if(!checkPostcode())
		bError = true;

	if(!checkEmail())
		bError = true;

	if(!checkEmailTwice())
		bError = true;

	if(!checkHousenumber())
		bError = true;





	//decide action
	if (bError && showError){
		$('.popup99').fadeIn();
		$('.popup-background').fadeIn();
		$('html, body').animate({
		        scrollTop: $('.popup99').offset().top - 10
		}, 500);

	}

	return !bError;
}*/



